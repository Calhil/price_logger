- remove duplicate db entries (db_remove_duplicates.py)
- database backup
- bter.dogecny.tradelist should use different coinbase, 100 is not enough
- testing
- http://docs.sqlalchemy.org/en/rel_0_9/orm/mapper_config.html
- add external commands
- automatic checking for new markets
- http://lowendbox.com/blog/getting-started-with-mysql-master-slave-replication/
- implement periodic trade frequency checking and adjust fetching time accordingly
- implement db connection pool, and connect/disconnect handling


Bugs:
----
- scheduler adds work twice when adding new exchange tables for the first time
- IOError 104 in dataprocessor when exitting with crtl+c
  IOError: [Errno 32] Broken pipe



Echanges:
---------
- bitstamp pusher api https://www.bitstamp.net/websocket/
- http://www.btc38.com
- cryptsy.com (ticker?)
- cryptsy.com pusher api
- bitmarket.pl (ticker, orderbook)
- vircurex (https://vircurex.com/welcome/api?locale=en)
- atomic-trade
- vault of satoshi (https://www.vaultofsatoshi.com/api)
- huobi
- www.coins-e.com


OTHER:
------
- http://datacommunitydc.org/blog/2014/03/building-data-apps-python/


DONE:
-----
- implement orderbook fetching
- error catching
- change trade_freq to fetch_freq in the conf file
- add logging
- apply correct logging levels DEBUG -> INFO, etc.
- implement tradelist fetching
- btce: btcusd coinbase fix
- split database into a package
- Exchanges: bitmarket, btc-e, bitstamp, bitcurex
- check tradelist data indices
- async/threaded fetching and sql queue
- biginteger for ammount
- bter fetching missing transactions from particular TID
- add database migration tools (db_migrate.py)
- calculate average transaction interval for each exchange/currency pair
- different delays for exchanges and api calls
    - compare timestamps now and from the last fetch
      if they are different than a specified value, fetch data
    - Added scheduler worker
- fetcher responding to bad status codes
- poison pill
- store next fetch time in db
- run_report.py: simple script to report number of transactions and market volume
- fix db connection timeouts
- find the cause of hang ups (after ~3 full rounds w/o scheduler)
- logging errors to a file -> use 'screen -L'
- create a proper api structure for all exchanges
- added shared dict with last tid for every tradelist
  removed any connections to db from fetcher, it now uses shared dict
- removed requests connection timeout errors


ADDED EXCHANGES:
----------------
- cryptsy (tradelist, orderbook)
- bter (tradelist)
- bitcurex (tradelist, ticker)
- btce (tradelist, ticker, orderbook)
- bitstamp (tradelist, ticker)
- bitmarket.pl (tradelist, ticker)
- poloniex (tradelist)
- bitfinex (orderbook, tradelist)
- okcoin
- btchina
- mintpal.com (tradelist, orderbook)
- huobi (ticker)
