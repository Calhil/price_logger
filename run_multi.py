import multiprocessing
import logging

from pricelogger.workers import Fetcher, DataProcessor, Scheduler
from config import URLS
from config import EXCHANGES_ALLOWED


if __name__ == "__main__":

    try:
        # create queues
        logging.info('Creating queues.')
        queue_work = multiprocessing.JoinableQueue()
        queue_results = multiprocessing.JoinableQueue()

        # create shared dict with last tid using manager
        mgr = multiprocessing.Manager()
        d = {}
        d_mgr = mgr.dict(d)

        # create workers
        logging.info('Initializing workers.')
        #f = [Fetcher(queue_work, queue_results) for ii in xrange(
        #    multiprocessing.cpu_count())]
        f = [Fetcher(queue_work, queue_results, d_mgr) for ii in xrange(12)]
        #f = [Fetcher(queue_work, queue_results)]
        #p = [processor(queue_results, queue_db) for ii in xrange(
        #    multiprocessing.cpu_count())]
        p = [DataProcessor(queue_results, d_mgr)]
        s = Scheduler(queue_work, d_mgr, URLS, exchanges_allowed = EXCHANGES_ALLOWED)

        # start the workers
        logging.info('Starting workers.')
        for iF in f:
            iF.start()
        for iP in p:
            iP.start()
        s.start()

        for iP in p:
            iP.join()
        for iF in f:
            iF.join()
        s.join()
        mgr.join()

    except KeyboardInterrupt:
        logging.warning('Shutting down and stopping workers.')
        for iP in p:
            iP.join()
        for iF in f:
            iF.join()
        s.join()
        mgr.join()
