Description
===========

Application used to log cryptocurrency prices from different exchanges

Requirements
------------

- SQLAlchemy
- mysql-python
- requests
- sqlalchemy-migrate
- pytz
- tzlocal

- mysql needs libmysqlclient-dev in virtualenv
