import logging
from decimal import Decimal
from datetime import datetime
try:
    from tzlocal import get_localzone
except:
    logging.warning('tzlocal not available')
from pytz import timezone, utc
from time import mktime

from config import SATOSHI, CENT


# timezones
try:
    tz_server = {
        'cryptsy': timezone('US/Eastern'),
        'poloniex': utc
    }
    tz_local = get_localzone()
except:
    pass


def ChooseCoinbase(currency_pair):
    # TODO: probably regex needed here
    # TODO: CNY needs higher precision
    coinbase = None
    if 'USD' in currency_pair or \
            'PLN' in currency_pair or\
            'CNY' in currency_pair or\
            'EUR' in currency_pair:
        coinbase = CENT
    else:
        coinbase = SATOSHI
    logging.debug('Coinbase chosen: %s for %s' % (coinbase, currency_pair))

    return coinbase


def Tradelist_Modify_Values(data, currency_pair, exchange):
    """ Modifies the tradelist values, changes coinbase, time format, etc. """

    COINBASE = ChooseCoinbase(currency_pair)

    # apply the correct coinbase and bid/ask values
    if type(data) is list:
        for ii in xrange(len(data)):
            for name, value in data[ii].items():
                if name in ['price', 'amount2']:
                    data[ii][name] = int(Decimal(value) * COINBASE)
                if name in ['amount']:
                    data[ii][name] = int(Decimal(value) * SATOSHI)
                if name in ['operation'] and exchange in \
                        ['btce', 'btcchina', 'bter', 'bitfinex', 'cryptsy',
                         'poloniex', 'mintpal', 'okcoin']:
                    if value in ['bid', 'buy', 'Buy', 'BUY']:
                        data[ii][name] = 2  # ask, this is according to bitcurex
                    else:
                        data[ii][name] = 1  # bid

                # str -> int for transaction id
                if name in ['tid']:
                    data[ii][name] = int(value)

                # exchange specific changes
                if exchange in ['cryptsy', 'poloniex']:
                    if name == 'timestamp':
                        datetime_object = datetime.strptime(
                            value, '%Y-%m-%d %H:%M:%S')
                        data[ii][name] = int(mktime(tz_server[exchange].localize(
                            datetime_object).astimezone(tz_local).timetuple()))
#                if exchange in ['mintpal']:
#                    if name == 'timestamp':
#                        data[ii]['tid'] = int(10000. * float(value))
#                        data[ii]['timestamp'] = int(value)
    else:
        logging.warning('ModifyTradelist got wrong variable.'
                        'type = {0}'.format(type(data)))

    return data


def Tradelist_Modify_Keys(data, exchange=None):
    """ Applies the correct dictionary keys. """

    logging.debug('Fixing dictionary keys')
    if type(data) is list:
        for ii in xrange(len(data)):
            # TODO: theres probably a more elegant way to do this
            if type(data[ii]) is dict and None not in data[ii].values():
                try:
                    if 'date' in data[ii]:
                        #print 'Data key in tradelist:', data[ii]
                        data[ii][u'timestamp'] = data[ii].pop('date')
                    if 'time' in data[ii]:
                        if exchange in ['mintpal']:
                            tmp = data[ii].pop('time')
                            data[ii][u'timestamp'] = int(float(tmp))
                            data[ii][u'tid'] = int(10000. * float(tmp))
                        else:
                            data[ii][u'timestamp'] = int(float(data[ii].pop('time')))
                    if 'datetime' in data[ii]:
                        data[ii][u'timestamp'] = data[ii].pop('datetime')
                    if 'TimeStamp' in data[ii]:
                        data[ii][u'timestamp'] = data[ii].pop('TimeStamp')

                    if 'type' in data[ii]:
                        data[ii][u'operation'] = data[ii].pop('type')
                    if 'Type' in data[ii]:
                        data[ii][u'operation'] = data[ii].pop('Type')
                    if 'initiate_ordertype' in data[ii]:
                        data[ii][u'operation'] = data[ii].pop('initiate_ordertype')

                    if 'quantity' in data[ii]:
                        data[ii]['amount'] = data[ii].pop('quantity')
                    if 'ammount' in data[ii]:
                        data[ii]['amount'] = data[ii].pop('ammount')  # bitcurex
                    if 'Amount' in data[ii]:
                        data[ii]['amount'] = data[ii].pop('Amount')
                    if 'total' in data[ii]:
                        data[ii]['amount2'] = data[ii].pop('total')

                    if 'tradeid' in data[ii]:
                        data[ii]['tid'] = int(data[ii].pop('tradeid'))
                    if 'tradeID' in data[ii]:
                        data[ii]['tid'] = int(data[ii].pop('tradeID'))
                    if 'ID' in data[ii]:
                        data[ii]['tid'] = int(data[ii].pop('ID'))

                    if 'Price' in data[ii]:
                        data[ii]['price'] = data[ii].pop('Price')
                    if 'tradeprice' in data[ii]:
                        data[ii]['price'] = data[ii].pop('tradeprice')
                    if 'rate' in data[ii]:
                        data[ii]['price'] = data[ii].pop('rate')

                except Exception as e:
                    logging.warning('Error occurred during data postprocessing: <{}>'.format(e))
    return data


def Ticker_Modify(data, currency_pair, exchange=None):
    """
    {"ticker": {
        "high":"55.42",
        "low":"52.17",
        "last":"52.71",
        "vol":286191.9909,
        "buy":"52.71",
        "sell":"52.83"},
    "time":1404256175}
    """
    data_tmp = None

    if type(data) is dict:
        data_tmp = {}

        COINBASE = ChooseCoinbase(currency_pair)

        for i_key, i_val in data.iteritems():
            if i_key in ('last'):
                data_tmp['price'] = int(float(i_val) * COINBASE)
            if i_key in ('vol'):
                data_tmp['amount'] = int(float(i_val) * SATOSHI)
            if i_key in ('buy'):
                data_tmp['bid'] = int(float(i_val) * COINBASE)
            if i_key in ('sell'):
                data_tmp['ask'] = int(float(i_val) * COINBASE)
            if i_key in ('time'):
                data_tmp['timestamp'] = int(i_val)
            if i_key in ('high'):
                data_tmp['high'] = int(float(i_val) * COINBASE)
            if i_key in ('low'):
                data_tmp['low'] = int(float(i_val) * COINBASE)
    else:
        logging.warning('Ticker_Modify: got wrong data type. data=<{0}>'.format(data))
    return data_tmp
