from requests import post
from requests import get
from requests import codes
from requests.models import ChunkedEncodingError
from requests.exceptions import Timeout
from requests import ConnectionError
import socket   # for timeout error, which requests wont catch
import logging
from time import time
from time import sleep
import hmac
from hashlib import sha512
from urllib import urlencode
from ssl import SSLError
#from decimal import Decimal

from config import CRYPTSY_MARKETID
from config import FETCHER_DELAY_ON_ERROR
from config import FETCHER_MAX_RETRIES
from config import BTCE_TRADES_LIMIT
from config import SATOSHI
from config import REQUESTS_TIMEOUT
from api_keys import CRYPTSY_SECRET_KEY
from api_keys import CRYPTSY_PUBLIC_KEY
from api_keys import POLONIEX_SECRET_KEY
from api_keys import POLONIEX_PUBLIC_KEY

from postprocessing import Tradelist_Modify_Values, Tradelist_Modify_Keys
from postprocessing import ChooseCoinbase
from postprocessing import Ticker_Modify

#from config import urls as url


def get_data_get(
        url, exchange, currency_pair, api_method,
        params={}, headers={}):
    """ Returns a list given json formatted response
    Uses requests.get method """
    response_json = None
    headers['Connection'] = 'close'
    for ii in range(0, FETCHER_MAX_RETRIES):
        response = None
        try:
            response = get(url, params=params, headers=headers, timeout=REQUESTS_TIMEOUT)
            response.connection.close()
        except (Timeout, ConnectionError, SSLError, ChunkedEncodingError, socket.timeout) as e:
            logging.warning('{0}.{1}.{2}: {3}: <{4}>'.format(
                exchange, currency_pair, api_method,
                type(e).__name__, e.message)
            )
        finally:
            if response is not None and response.status_code == codes.ok:
                try:
                    response_json = response.json()
                    break
                except ValueError as e:
                    # JSONDecodeError
                    logging.warning(
                        '{0}.{1}.{2}: JSONDecodeError: <{3}>'.format(
                            exchange, currency_pair, api_method, e.message))
            else:
                #break
                logging.warning(
                    '{0}.{1}.{2}: Invalid response, discarding data'
                    .format(exchange, currency_pair, api_method)
                )
                #break
            sleep(FETCHER_DELAY_ON_ERROR)
    if response_json is None:
        # max tries exceeded with no results
        logging.warning(
            '{0}.{1}.{2}: No response received after {3} attempts'
            .format(exchange, currency_pair, api_method, FETCHER_MAX_RETRIES)
        )
    return response_json


def get_data_post(
        url, payload, exchange, currency_pair, api_method,
        params={}, headers={}):
    """ Returns a list given json formatted response
    Uses requests.post method """
    response_json = None
    headers['Connection'] = 'close'
    for ii in range(0, FETCHER_MAX_RETRIES):
        response = None
        try:
            response = post(url, payload, params=params, headers=headers, timeout=REQUESTS_TIMEOUT)
            response.connection.close()
        except (Timeout, ConnectionError, SSLError, ChunkedEncodingError) as e:
            logging.warning('{0}.{1}.{2}: {3}: <{4}>'.format(
                exchange, currency_pair, api_method,
                type(e).__name__, e.message)
            )
        finally:
            if response is not None and response.status_code == codes.ok:
                try:
                    response_json = response.json()
                    break
                except ValueError as e:
                    # JSONDecodeError
                    logging.warning(
                        '{0}.{1}.{2}: JSONDecodeError: <{3}>'.format(
                            exchange, currency_pair, api_method, e.message))
            else:
                #break
                logging.warning(
                    '{0}.{1}.{2}: Invalid response, retrying'
                    .format(exchange, currency_pair, api_method)
                )
                sleep(FETCHER_DELAY_ON_ERROR)
    if response_json is None:
        # max tries exceeded with no results
        logging.warning(
            '{0}.{1}.{2}: No response received after {3} attempts'
            .format(exchange, currency_pair, api_method, FETCHER_MAX_RETRIES)
        )
    return response_json


def _not_implemented():
    logging.error('Method not implemented')
    return


class cryptsy():

    def __init__(self, url, *args, **kwargs):
        self.url = url
        self._secret_key = CRYPTSY_SECRET_KEY
        self._public_key = CRYPTSY_PUBLIC_KEY
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query_private(self, api_method, params={}):
        # private api
        nonce = int(100 * time())

        if api_method == 'tradelist':
            api_method_cryptsy = 'markettrades'
        elif api_method == 'orderbook':
            api_method_cryptsy = 'marketorders'
        else:
            _not_implemented()

        payload = {
            'marketid': CRYPTSY_MARKETID[self.currency_pair],
            'method': api_method_cryptsy,
            'nonce': nonce
        }
        # signing message
        hasher = hmac.new(self._secret_key.encode(), digestmod=sha512)
        hasher.update(urlencode(payload).encode())
        sign = hasher.hexdigest()
        headers = {
            'Key': self._public_key,
            'Sign': sign,
            'Content-type': 'application/json',
            'Accept': 'text/javascript, text/html, application/xml, '
                      'text/xml, application/json */*',
            'Accept-Charset': 'utf-8'
        }

        # fetch data
        # TODO implement some kind of communication with scheduler
        #      in case no data was received
        response_json = get_data_post(
            self.url, payload, 'cryptsy',
            self.currency_pair, api_method,
            params=params, headers=headers)

        # check for errors in data
        try:
            if response_json['success'] != '1':
                logging.warning(
                    'cryptsy._api_query_private error: <{0}>'
                    .format(response_json['error'])
                )
        except TypeError:
            logging.warning('cryptsy.{0}.{1}: response_json = None'.format(
                self.currency_pair, api_method))
        return response_json

    def _post_processing(self, api_method, data):
        data_tmp = None
        if data is not None:
            try:
                data_tmp = data['return']
                if api_method == 'tradelist':
                    # fix keys
                    # fix values
                    data_tmp = Tradelist_Modify_Values(
                        Tradelist_Modify_Keys(data_tmp),
                        self.currency_pair,
                        'cryptsy')
                elif api_method == 'orderbook':
                    #data_tmp['bids'] = data_tmp.pop('buyorders')
                    #data_tmp['asks'] = data_tmp.pop('sellorders')

                    data_new = {'bids': [], 'asks': []}
                    for i_key in data_tmp.keys():

                        if i_key == 'buyorders':
                            key1 = 'bids'
                            key2 = 'buyprice'
                        else:
                            key1 = 'asks'
                            key2 = 'sellprice'

                        for i_order in data_tmp[i_key]:
                            data_new[key1].append([
                                int(float(i_order[key2]) * SATOSHI),
                                int(float(i_order['quantity']) * SATOSHI)])
                    data_tmp = data_new
                else:
                    logging.error('_post_processing method not implemented '
                                  'for {0}'.format(api_method))
            except KeyError as e:
                logging.warning('cryptsy.{0}.{1} _post_processing: <{2}>'.format(
                    self.currency_pair, api_method, e.message))
                data_tmp = None
        return data_tmp

    def getticker(self):
        pass

    def getticker_data(self):
        pass

    def gettradelist(self):
        #TODO check if currency_pair was specified
        return self._api_query_private('tradelist')

    def gettradelist_data(self):
        return self._post_processing('tradelist', self.gettradelist())

    def getorderbook(self):
        return self._api_query_private('orderbook')

    def getorderbook_data(self):
        return self._post_processing('orderbook', self.getorderbook())


class crxzone():

    def __init__(self, url, *args, **kwargs):
        self.url = url
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query_private(self):
        _not_implemented()

    def _api_query_public(self, api_method, params={}, headers={}):
        response_json = None
        if api_method in ['tradelist']:
            # {"ErrorMessage":null,"IsSuccess":true,"Result":[]}
            response_json = get_data_get(
                self.url, 'crxzone', self.currency_pair, api_method,
                params=params, headers=headers)
        else:
            return _not_implemented()

        return response_json

    def _post_processing(self, api_method, data):
        data_tmp = None
        if data is not None:
            if api_method in ['tradelist']:
                # extract data
                if data['IsSuccess'] == True and \
                        data['ErrorMessage'] == None:
                    data = data['Result']
                    #print 'crxzone api: _post_processing, data=<{}>'.format(data)

                    data_tmp = Tradelist_Modify_Values(
                        Tradelist_Modify_Keys(data), self.currency_pair, 'crxzone')
                else:
                    logging.warning('crxzone._post_processing: <{}>'.format(data['ErrorMessage']))
            else:
                logging.error('_post_processing method not implemented'
                              'for {0}'.format(api_method))
                _not_implemented()
        return data_tmp

    def getticker(self):
        _not_implemented()

    def getticker_data(self):
        _not_implemented()

    def gettradelist(self):
        return self._api_query_public('tradelist')

    def gettradelist_data(self):
        return self._post_processing('tradelist',
                                     self.gettradelist())

    def getorderbook(self):
        _not_implemented()

    def getorderbook_data(self):
        _not_implemented()


class btce():

    def __init__(self, url, *args, **kwargs):
        self.url = url
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query_private(self):
        return _not_implemented()

    def _api_query_public(self, api_method, params={}, headers={}):
        # TODO: catch error messages {"error": '<message>'}
        if api_method == 'tradelist':
            response_json = get_data_get(
                self.url, 'btce', self.currency_pair, api_method,
                params=params, headers=headers)
        elif api_method == 'orderbook':
            response_json = get_data_get(
                self.url, 'btce', self.currency_pair, api_method)
        else:
            _not_implemented()

        return response_json

    def _post_processing(self, api_method, data):
        data_tmp = None
        if data is not None:
            # extract the list with transactions
            data_tmp = data[data.keys()[0]]

            if api_method == 'tradelist':
                # fix keys
                # fix values
                # {u'amount': u'0.18183478',
                # u'date': u'2014-05-25 23:31:20',  -> timestamp
                # u'rate': u'0.01961011',           -> price
                # u'total': u'0.00356580',          -> amount2
                # u'tradeID': u'22235',             -> tid
                # u'type': u'buy'}                  -> operation
                data_tmp = Tradelist_Modify_Values(
                    Tradelist_Modify_Keys(data_tmp), self.currency_pair, 'btce')
            elif api_method == 'orderbook':
                # apply correct currency base
                COINBASE = ChooseCoinbase(self.currency_pair)
                for i_key in data_tmp.keys():
                    for i_order in xrange(len(data_tmp[i_key])):
                        data_tmp[i_key][i_order] = \
                            [int(float(data_tmp[i_key][i_order][0]) * COINBASE),
                             int(float(data_tmp[i_key][i_order][1]) * SATOSHI)]
            else:
                _not_implemented()
        return data_tmp

    def gettradelist(self):
        return self._api_query_public(
            api_method='tradelist', params={'limit': BTCE_TRADES_LIMIT})

    def gettradelist_data(self):
        return self._post_processing(
            api_method='tradelist', data=self.gettradelist())

    def getorderbook(self):
        return self._api_query_public('orderbook')

    def getorderbook_data(self):
        return self._post_processing('orderbook', self.getorderbook())


class bitcurex():
    # TODO add some kind of url verification in the public api url

    def __init__(self, url, *args, **kwargs):
        self.url = url
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query_private(self):
        _not_implemented()

    def _api_query_public(self, api_method, params={}, headers={}):
        response_json = None
        if api_method in ['tradelist']:
            response_json = get_data_get(
                self.url, 'bitcurex', self.currency_pair, api_method,
                params=params, headers=headers)
        else:
            return _not_implemented()

        return response_json

    def _post_processing(self, api_method, data):
        data_tmp = None
        if data is not None:
            if api_method in ['tradelist']:
                data_tmp = Tradelist_Modify_Values(
                    Tradelist_Modify_Keys(data), self.currency_pair, 'bitcurex')
            else:
                logging.error('_post_processing method not implemented'
                              'for {0}'.format(api_method))
                _not_implemented()
        return data_tmp

    def getticker(self):
        _not_implemented()

    def getticker_data(self):
        _not_implemented()

    def gettradelist(self):
        return self._api_query_public('tradelist')

    def gettradelist_data(self):
        return self._post_processing('tradelist',
                                     self.gettradelist())

    def getorderbook(self):
        _not_implemented()

    def getorderbook_data(self):
        _not_implemented()


class bitfinex():

    def __init__(self, url, *args, **kwargs):
        self.url = url
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query_private(self):
        return _not_implemented()

    def _api_query_public(self, api_method, params={}, headers={}):
        response_json = None

        if api_method == 'orderbook' or api_method == 'tradelist':
            response_json = get_data_get(
                self.url, 'bitfinex', self.currency_pair, api_method,
                params=params, headers=headers)
        else:
            return _not_implemented()
        return response_json

    def _post_processing(self, api_method, data):
        data_tmp = None
        if data is not None:
            if api_method == 'orderbook':
                # {"bids": [
                #   {"price":"0.017181",
                #    "amount":"9.4",
                #    "timestamp":"1402152327.0"}, ...,  {}]
                #
                coinbase = ChooseCoinbase(self.currency_pair)
                data_tmp = {'bids': [], 'asks': []}
                for i_key in data.keys():
                    for ii in xrange(0, len(data[i_key])):
                        data_tmp[i_key].append(
                            [int(float(data[i_key][ii]['price']) * coinbase),
                             int(float(data[i_key][ii]['amount']) * SATOSHI)])
            elif api_method == 'tradelist':
                data_tmp = Tradelist_Modify_Values(
                    Tradelist_Modify_Keys(data),
                    self.currency_pair,
                    'bitfinex')
                # TODO check if this is necessary
                # remove unnecessary data
                for ii in xrange(0, len(data_tmp)):
                    data_tmp[ii].pop('exchange', None)
            else:
                return _not_implemented()
        return data_tmp

    def getorderbook(self):
        return self._api_query_public('orderbook')

    def getorderbook_data(self):
        return self._post_processing('orderbook', self.getorderbook())

    def gettradelist(self):
        return self._api_query_public('tradelist')

    def gettradelist_data(self):
        return self._post_processing('tradelist', self.gettradelist())


class bter():

    def __init__(self, url, last_transaction_id=None, *args, **kwargs):
        self.url = url
        self.last_tid = last_transaction_id
        if self.last_tid is not None:
            self.url = self.url + '/%s' % (self.last_tid)
        else:
            self.url = self.url + '/1'

        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query(self, api_method, params={}, headers={}):
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        response_json = None

        if api_method in ['ticker', 'tradelist', 'orderbook']:
            response_json = get_data_get(
                self.url,
                'bter', self.currency_pair, api_method, params=params, headers=headers)
        else:
            return _not_implemented()

        return response_json

    def _post_processing(self, api_method, data):
        data_tmp = None
        if data is not None:
            try:
                data_tmp = data['data']
            except:
                logging.warning('bter.{0}.{1}: _post_processing received None data'.format(
                    self.currency_pair, api_method))
            if api_method in ['tradelist']:
                if data is not None:
                    #FIXME wywala sie data['data'] nie dziala
                    if 'data' in data:
                        data_tmp = Tradelist_Modify_Values(
                            Tradelist_Modify_Keys(data['data']),
                            self.currency_pair, 'bter')
            else:
                _not_implemented()

        return data_tmp

    def getticker(self):
        _not_implemented()

    def getticker_data(self):
        _not_implemented()

    def gettradelist(self):
        return self._api_query('tradelist')

    def gettradelist_data(self):
        return self._post_processing('tradelist',
                                     self.gettradelist())


class bitstamp():

    def __init__(self, url, *args, **kwargs):
        self.url = url
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query_public(self, api_method, params={}, headers={}):
        response_json = None
        if api_method in ['tradelist']:
            response_json = get_data_get(
                self.url, 'bitstamp', self.currency_pair, api_method,
                params=params, headers=headers)
        else:
            return _not_implemented()
        return response_json

    def _api_query_private(self):
        return _not_implemented()

    def _post_processing(self, api_method, data):
        data_tmp = None
        if data is not None:
            if api_method in ['tradelist']:
                data_tmp = Tradelist_Modify_Values(
                    Tradelist_Modify_Keys(data), self.currency_pair, 'bitstamp')
            else:
                logging.error('bitstamp.{0}.{1}: _post_processing method not implemented '
                              'for {1}'.format(self.currency_pair, api_method))
        return data_tmp

    def gettradelist(self):
        return self._api_query_public('tradelist')

    def gettradelist_data(self):
        return self._post_processing('tradelist',
                                     self.gettradelist())


class bitmarket():

    def __init__(self, url, last_transaction_id=None, *args, **kwargs):
        self.url = url
        self.last_tid = last_transaction_id
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query_public(self, api_method, params={}, headers={}):
        response_json = None
        if api_method in ['tradelist']:
            # update params if necessary
            if self.last_tid is not None:
                params.update({'since': self.last_tid})
            else:
                logging.warning('bitmarket.{0}.{1}: _api_query_public invalid '
                                'transaction id.'.format(
                                    self.currency_pair,
                                    api_method))
            response_json = get_data_get(
                self.url, 'bitmarket', self.currency_pair, api_method,
                params=params, headers=headers)
        else:
                logging.error('bitmarket.{0}.{1}_api_query_public method error'.format(
                    self.currency_pair, api_method))
        return response_json

    def _api_query_private(self, api_method, params={}, headers={}):
        return _not_implemented()

    def _post_processing(self, api_method, data):
        data_tmp = None
        if data is not None:
            if api_method in ['tradelist']:
                data_tmp = Tradelist_Modify_Values(
                    Tradelist_Modify_Keys(data),
                    self.currency_pair,
                    'bitmarket')
            else:
                return _not_implemented()
        return data_tmp

    def gettradelist(self):
        return self._api_query_public('tradelist')

    def gettradelist_data(self):
        return self._post_processing('tradelist', self.gettradelist())


class kraken():

    def __init__(self, url, *args, **kwargs):
        self.url = url
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                # non standard currency pair naming
                if key == 'currency_pair':
                    if value == 'LTCBTC':
                        value = 'XXBTXLTC'
                    setattr(self, key, value)
                else:
                    setattr(self, key, value)

    def _api_query_private(self, api_method, params={}, headers={}):
        return _not_implemented()

    def _api_query_public(self, api_method, params={}, headers={}):
        response_json = None
        if api_method == 'orderbook':
            params['pair'] = self.currency_pair
            #params['count'] = 50    #  TODO  non default value
            response_json = get_data_get(
                self.url, 'kraken', self.currency_pair, api_method,
                params=params, headers=headers)
        else:
            return _not_implemented()

        return response_json

    def _post_processing(self, api_method, data):
        data_tmp = None
        if data is not None:
            if api_method == 'orderbook':
                # extract the bis/ask dictionary
                data_tmp = data['result'][self.currency_pair]
                data_new = {'bids': [], 'asks': []}
                for tmp_key in data_tmp.keys():
                    if tmp_key == 'bids':
                        i_key = 'asks'
                    elif tmp_key == 'asks':
                        i_key = 'bids'
                    else:
                        logging.warning('Kraken._post_processing: Unknown orderbook key.')

                    for ii in xrange(0, len(data_tmp[i_key])):
                        data_new[i_key].append(
                            [int(1 / float(data_tmp[i_key][ii][0]) * SATOSHI),
                             int(float(data_tmp[i_key][ii][1]) * SATOSHI)])
                data_tmp = data_new
            else:
                return _not_implemented()
        return data_tmp

    def getorderbook(self):
        return self._api_query_public('orderbook')

    def getorderbook_data(self):
        return self._post_processing('orderbook', self.getorderbook())


class poloniex():

    def __init__(self, url, *args, **kwargs):
        self.url = url
        self._secret_key = POLONIEX_SECRET_KEY
        self._public_key = POLONIEX_PUBLIC_KEY
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query_private(self):
        return _not_implemented()

    def _api_query_public(self, api_method):
        # TODO: catch error messages {"error": '<message>'}
        if api_method == 'tradelist':
            response_json = get_data_get(
                self.url, 'poloniex', self.currency_pair, api_method)
            #logging.warning('poloniex type response_json = {}'.format(type(response_json)))
            #logging.warning('poloniex response_json = {}'.format(response_json))
            if type(response_json) is dict and response_json.get('error'):
                logging.warning('poloniex.{}.{}: json error: <{}>'.format(
                    self.currency_pair, api_method,
                    response_json['error']))
                response_json = None
        return response_json

    def _post_processing(self, api_method, data):
        data_tmp = None
        if data is not None:
            if api_method in ['tradelist']:
                # fix keys
                # fix values
                # {u'amount': u'0.18183478',
                # u'date': u'2014-05-25 23:31:20',  -> timestamp
                # u'rate': u'0.01961011',           -> price
                # u'total': u'0.00356580',          -> amount2
                # u'tradeID': u'22235',             -> tid
                # u'type': u'buy'}                  -> operation
                # utc time

                data_tmp = Tradelist_Modify_Values(
                    Tradelist_Modify_Keys(data), self.currency_pair, 'poloniex')
            else:
                _not_implemented()
        return data_tmp

    def gettradelist(self):
        return self._api_query_public(api_method='tradelist')

    def gettradelist_data(self):
        return self._post_processing(
            api_method='tradelist', data=self.gettradelist())


class mintpal():

    def __init__(self, url, *args, **kwargs):
        self.url = url
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query_private(self):
        return _not_implemented()

    def _api_query_public(self, api_method):
        if api_method == 'tradelist':
            response_json = get_data_get(
                self.url, 'mintpal', self.currency_pair, api_method)
            # api v2 ONLY
            #if response_json['status'] != 'success':
            #    logging.warning(
            #        'mintpal._api_query_public error: <{0}>'
            #        .format(response_json['message']))
        elif api_method == 'orderbook':
            bids = get_data_get(self.url + '/BUY', 'mintpal', self.currency_pair, api_method)
            asks = get_data_get(self.url + '/SELL', 'mintpal', self.currency_pair, api_method)

            try:
                if bids['status'] == 'success' and asks['status'] == 'success':
                    response_json = {
                        'asks': asks['data'],
                        'bids': bids['data']}
                else:
                    response_json = None
            except TypeError:
                logging.warning('mintpal.{0}.{1} none type error'.format(
                    self.currency_pair, api_method))
                response_json = None
        else:
            _not_implemented()

        return response_json

    def _post_processing(self, api_method, data):
        data_tmp = None
        if data is not None:
            if api_method == 'tradelist':
                #  "type":"SELL",               -> operation
                #  "price":"0.00000023",
                #  "amount":"412128.80177019",
                #  "total":"0.09478962",        -> amount2
                #  "time":"1394498289.2727"     -> timestamp
                data_tmp = data['trades']
                data_tmp = Tradelist_Modify_Values(
                    Tradelist_Modify_Keys(data_tmp, exchange='mintpal'),
                    self.currency_pair,
                    'mintpal')
            elif api_method == 'orderbook':
                # {"price":"0.01714998","amount":"33.71090019","total":"0.57814126"}
                data_tmp = {'bids': [], 'asks': []}

                for i_key in data.keys():
                    for ii in xrange(0, len(data[i_key])):
                        data_tmp[i_key].append(
                            [int(float(data[i_key][ii]['price']) * SATOSHI),
                             int(float(data[i_key][ii]['amount']) * SATOSHI)])
            else:
                data_tmp = None
                logging.error('mintpal._post_processing method not implemented'
                              'for {0}'.format(api_method))
        return data_tmp

    def gettradelist(self):
        return self._api_query_public('tradelist')

    def gettradelist_data(self):
        return self._post_processing('tradelist', self.gettradelist())

    def getorderbook(self):
        return self._api_query_public('orderbook')

    def getorderbook_data(self):
        return self._post_processing('orderbook', self.getorderbook())


class okcoin ():

    def __init__(self, url, last_transaction_id=None, *args, **kwargs):
        self.url = url
        self.last_tid = last_transaction_id
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query_public(self, api_method, params={}, headers={}):
        response_json = None
        #params.update({'since': self.last_tid})

        if api_method in ['tradelist']:
            response_json = get_data_get(
                self.url,
                'okcoin', self.currency_pair, api_method, params=params, headers=headers)
        else:
            return _not_implemented()
        return response_json

    def _post_processing(self, api_method, data):
        data_tmp = None
        if data is not None:
            if api_method == 'tradelist':
                data_tmp = Tradelist_Modify_Values(
                    Tradelist_Modify_Keys(data),
                    self.currency_pair,
                    'okcoin')
        return data_tmp

    def gettradelist(self):
        return self._api_query_public(
            'tradelist',
            params={'since': self.last_tid})

    def gettradelist_data(self):
        return self._post_processing('tradelist', self.gettradelist())


class btcchina():

    def __init__(self, url, last_transaction_id=None, *args, **kwargs):
        self.url = url
        self.last_tid = last_transaction_id
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query_private(self):
        return _not_implemented()

    def _api_query_public(self, api_method, params={}, headers={}):
        response_json = None

        if api_method == 'tradelist':
            # update params if necessary
            if self.last_tid is not None:
                params.update({'since': self.last_tid})
            else:
                logging.warning('btcchina.{0}.{1}: _api_query_public invalid '
                                'transaction id.'.format(
                                    self.currency_pair,
                                    api_method))
            params.update({'market': self.currency_pair.lower()})

            response_json = get_data_get(
                self.url, 'btcchina', self.currency_pair, api_method,
                params=params, headers=headers)
        else:
            return _not_implemented()
        return response_json

    def _post_processing(self, api_method, data):
        data_tmp = None

        if data is not None:
            if api_method == 'tradelist':
                data_tmp = Tradelist_Modify_Values(
                    Tradelist_Modify_Keys(data),
                    self.currency_pair,
                    'btcchina')
        return data_tmp

    def gettradelist(self):
        return self._api_query_public('tradelist')

    def gettradelist_data(self):
        return self._post_processing(
            'tradelist',
            self.gettradelist())


class huobi():

    def __init__(self, url, last_transaction_id=None, *args, **kwargs):
        self.url = url
        self.last_tid = last_transaction_id
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                setattr(self, key, value)

    def _api_query_private(self):
        return _not_implemented()

    def _api_query_public(self, api_method, params={}, headers={}):
        response_json = None

        if api_method == 'ticker':
            response_json = get_data_get(
                self.url,
                'huobi',
                self.currency_pair,
                api_method,
                params=params,
                headers=headers)
        else:
            return _not_implemented()

        return response_json

    def _post_processing(self, api_method, data):
        data_tmp = None

        if data is not None:
            if api_method == 'ticker':
                # merge dictionary contents
                data.update(data.pop('ticker'))
                data_tmp = Ticker_Modify(data, self.currency_pair, 'huobi')
            else:
                return _not_implemented()

        return data_tmp

    def getticker(self):
        return self._api_query_public('ticker')

    def getticker_data(self):
        return self._post_processing(
            'ticker',
            self.getticker())
