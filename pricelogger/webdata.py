import requests
from time import sleep
from time import time
from hashlib import sha512
import hmac
from urllib import urlencode
import logging

from config import BTCE_TRADES_LIMIT
from api_keys import CRYPTSY_PUBLIC_KEY, CRYPTSY_SECRET_KEY
from config import CRYPTSY_MARKETID
from config import REQUESTS_TIMEOUT


def getJson(
        url, exchange=None, api_method=None, transaction_id=None,
        retryTime=5, currency_pair=None):
    """
    Retrieves json formatted data given an url.

    If 'exchange' parameter is passed, the function will modify resulting json
    """

    url, payload, headers = PrepareUrl(
        url, exchange, api_method, transaction_id, CurrencyPair=currency_pair)

    while True:
        connected = False
        try:
            if exchange == 'cryptsy':  # using cryptsy private api
                r = requests.post(url, payload, headers=headers, timeout=REQUESTS_TIMEOUT).json()
            else:
                if exchange != 'bter':
                    r = requests.get(url, params=payload, timeout=REQUESTS_TIMEOUT).json()
                else:
                    r = requests.get(url, timeout=REQUESTS_TIMEOUT).json()

        except Exception as e:
            logging.warning('Something failed, retrying in %s', retryTime)
            logging.warning('Error msg: %s', e.message)
            connected = False
            sleep(retryTime)
        else:
            connected = True

        if connected:
            break

    # extract the inner dictionary from json
    if exchange:
        if exchange in ('btce') and api_method in ('ticker', 'tradelist'):
            logging.debug('Modifying json for %s' % exchange)
            r = r.values()[0]

        if exchange in ('bter') and api_method in ('tradelist'):
            logging.debug('Modifying json for %s' % exchange)
            r = r['data']

        if exchange in ('cryptsy'):
            logging.debug('Modifying json for %s' % exchange)
            r = r['return']

    return r


def PrepareUrl(
        url, exchange, api_method, transaction_id,
        currency_pair=None, *args, **kwargs):
    """ Applies any fixes needed for particular exchange """
    payload = None
    headers = None

    # BTC-E
    if exchange == 'btce':
        #url = url + '?limit=%s' % (BTCE_TRADES_LIMIT)
        payload = {'limit': BTCE_TRADES_LIMIT}

    # bter
    if exchange == 'bter':
        if transaction_id is not None:
            url = url + '/%s' % (transaction_id)
        else:
            url = url + '/1'

    # cryptsy.com
    if exchange == 'cryptsy':
        nonce = int(time())
        #TODO: implement correct marketid
        payload = {
            'marketid': CRYPTSY_MARKETID[currency_pair],
            'method': 'markettrades',
            'nonce': nonce}

        # signing message
        hasher = hmac.new(CRYPTSY_SECRET_KEY.encode(), digestmod=sha512)
        hasher.update(urlencode(payload).encode())
        sign = hasher.hexdigest()

        headers = {'Key': CRYPTSY_PUBLIC_KEY, 'Sign': sign}

    return url, payload, headers
