#import logging
from sqlalchemy import Column, Integer, BigInteger

from base import Base


class BTCCNY():

    class ticker(Base):
        """
        {"ticker": {
            "high":"4055",
            "low":"3980",
            "last":"4031.04",
            "vol":68407.3737,
            "buy":"4030.31",
            "sell":"4031.01"},
        "time":1404256164}
        """

        __tablename__ = "huobi_btccny_ticker"
        id = Column(Integer, primary_key=True)
        price = Column(Integer)  # last
        amount = Column(BigInteger)  # vol
        high = Column(Integer)
        low = Column(Integer)
        bid = Column(Integer)
        ask = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, bid=%s, ask=%s, high=%s, '
            'low=%s, volume=%s, timestamp=%s>' % \
                (self.price, self.bid, self.ask, self.high, self.low,
                    self.amount, self.timestamp)

class LTCCNY():

    class ticker(Base):
        """
        {"ticker": {
            "high":"55.42",
            "low":"52.17",
            "last":"52.71",
            "vol":286191.9909,
            "buy":"52.71",
            "sell":"52.83"},
        "time":1404256175}
        """

        __tablename__ = "huobi_ltccny_ticker"
        id = Column(Integer, primary_key=True)
        price = Column(Integer)  # last
        amount = Column(BigInteger)  # vol
        high = Column(Integer)
        low = Column(Integer)
        bid = Column(Integer)
        ask = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, bid=%s, ask=%s, high=%s, '
            'low=%s, volume=%s, timestamp=%s>' % \
                (self.price, self.bid, self.ask, self.high, self.low,
                    self.amount, self.timestamp)

