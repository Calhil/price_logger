import bitcurex
import bitmarket
import bitstamp
import btce
import bter
import cryptsy
import models

from config import ENGINE_ECHO, SQLALCHEMY_DATABASE_URI

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
#import logging


__all__ = ['bitstamp',
           'bitcurex',
           'btcchina',
           'btce',
           'bitmarket',
           'bitfinex',
           'bter',
           'cryptsy',
           'crxzone',
           'huobi',
           'mintpal',
           'okcoin',
           'poloniex',
           'models']


# add database stuff
#logging.info("Initializing database.")

# single threaded
#engine = create_engine(SQLALCHEMY_DATABASE_URI, echo=ENGINE_ECHO)
#Session = sessionmaker(bind=engine)


# multithreaded
engine = create_engine(SQLALCHEMY_DATABASE_URI,
                       echo=ENGINE_ECHO,
                       pool_recycle=18000)

# for multiprocessing
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
