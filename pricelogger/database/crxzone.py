
from sqlalchemy import Column, Integer, BigInteger

from base import Base


class BTCUSD():
    class tradelist(Base):
        """ 
        {"ID":1126765,
         "Price":473.2900000000,
         "Amount":0.1202200000,
         "Type":1,
         "TimeStamp":1409552219}
        """
        __tablename__ = "crxzone_btcusd_tradelist"

        id = Column(Integer, primary_key=True)

        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, '\
                   'timestamp={}>'.format(self.price, self.amount, 
                           self.operation, self.tid, self.timestamp)


class BTCEUR():
    class tradelist(Base):
        """ 
        {"ID":1126765,
         "Price":473.2900000000,
         "Amount":0.1202200000,
         "Type":1,
         "TimeStamp":1409552219}
        """
        __tablename__ = "crxzone_btceur_tradelist"

        id = Column(Integer, primary_key=True)

        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, '\
                   'timestamp={}>'.format(self.price, self.amount, 
                           self.operation, self.tid, self.timestamp)


class BTCLTC():
    class tradelist(Base):
        """ 
        {"ID":1126765,
         "Price":473.2900000000,
         "Amount":0.1202200000,
         "Type":1,
         "TimeStamp":1409552219}
        """
        __tablename__ = "crxzone_btcltc_tradelist"

        id = Column(Integer, primary_key=True)

        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, '\
                   'timestamp={}>'.format(self.price, self.amount, 
                           self.operation, self.tid, self.timestamp)


class LTCUSD():
    class tradelist(Base):
        """ 
        {"ID":1126765,
         "Price":473.2900000000,
         "Amount":0.1202200000,
         "Type":1,
         "TimeStamp":1409552219}
        """
        __tablename__ = "crxzone_ltcusd_tradelist"

        id = Column(Integer, primary_key=True)

        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, '\
                   'timestamp={}>'.format(self.price, self.amount, 
                           self.operation, self.tid, self.timestamp)


class LTCEUR():
    class tradelist(Base):
        """ 
        {"ID":1126765,
         "Price":473.2900000000,
         "Amount":0.1202200000,
         "Type":1,
         "TimeStamp":1409552219}
        """
        __tablename__ = "crxzone_ltceur_tradelist"

        id = Column(Integer, primary_key=True)

        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, '\
                   'timestamp={}>'.format(self.price, self.amount, 
                           self.operation, self.tid, self.timestamp)


class LTCSGD():
    class tradelist(Base):
        """ 
        {"ID":1126765,
         "Price":473.2900000000,
         "Amount":0.1202200000,
         "Type":1,
         "TimeStamp":1409552219}
        """
        __tablename__ = "crxzone_ltcsgd_tradelist"

        id = Column(Integer, primary_key=True)

        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, '\
                   'timestamp={}>'.format(self.price, self.amount, 
                           self.operation, self.tid, self.timestamp)


class BTCSGD():
    class tradelist(Base):
        """ 
        {"ID":1126765,
         "Price":473.2900000000,
         "Amount":0.1202200000,
         "Type":1,
         "TimeStamp":1409552219}
        """
        __tablename__ = "crxzone_btcsgd_tradelist"

        id = Column(Integer, primary_key=True)

        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, '\
                   'timestamp={}>'.format(self.price, self.amount, 
                           self.operation, self.tid, self.timestamp)
