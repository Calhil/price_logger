import logging
from sqlalchemy import Column, Integer, BigInteger

from base import Base

#class bitmarket():

class BTCPLN():

    class ticker(Base):
        """
        {"ask":1795.9,"bid":1794,"last":1794,"low":1750,"high":1840.12,"vwap":1792.5514219496,"volume":56.69787944}
        """
        __tablename__ = "bitmarket_btcpln_ticker"

        id = Column(Integer, primary_key=True)
        last = Column(Integer)
        bid = Column(Integer)
        ask = Column(Integer)
        high = Column(Integer)
        low = Column(Integer)
        volume = Column(BigInteger)
        vwap = Column(Integer)

        def __init__(self, *args, **kwargs):
            logging.debug('bitmarket.btcpln.ticker, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)


        def __repr__(self):
            return '<last=%s, bid=%s, ask=%s, high=%s, '
            'low=%s, volume=%s, vwap=%s>' % \
                (self.last, self.bid, self.ask, self.high, self.low,
                    self.volume, self.vwap)


    class tradelist(Base):
        """
        {"amount":0.05677076,"price":1788,"date":1395419184,"tid":8699}
        """
        __tablename__ = "bitmarket_btcpln_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)


        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount,
                    self.tid, self.timestamp)


class LTCPLN():

    class ticker(Base):
        """
        {"ask":51.48,"bid":49.53,"last":49.53,"low":49.5,"high":56.2499,"vwap":50.985846213523,"volume":1035.44529669}
        """
        __tablename__ = "bitmarket_ltcpln_ticker"

        id = Column(Integer, primary_key=True)
        last = Column(Integer)
        bid = Column(Integer)
        ask = Column(Integer)
        high = Column(Integer)
        low = Column(Integer)
        volume = Column(BigInteger)
        vwap = Column(Integer)

        def __init__(self, *args, **kwargs):
            logging.debug('bitmarket.ltcpln.ticker, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)


        def __repr__(self):
            return '<last=%s, bid=%s, ask=%s, high=%s, '
            'low=%s, volume=%s, vwap=%s>' % \
                (self.last, self.bid, self.ask, self.high, self.low,
                    self.volume, self.vwap)


    class tradelist(Base):
        """
        {"amount":7.74880365,"price":50,"date":1395418057,"tid":2471}
        """
        __tablename__ = "bitmarket_ltcpln_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)


        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount,
                    self.tid, self.timestamp)



