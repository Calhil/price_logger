import logging
from sqlalchemy import Column, Integer, BigInteger

from base import Base

#class bitcurex():


class BTCPLN():

    class ticker(Base):
        """ {"high": 1969, "low": 1835.87, "avg": 1902.435,
        "vwap": 1875.33303472, "vol": 285.42050599, "last": 1890, "buy": 1889,
        "sell": 1890, "time": 1395161353} """

        __tablename__ = "bitcurex_btcpln_ticker"

        id = Column(Integer, primary_key=True)
        last = Column(Integer)
        bid = Column(Integer)
        ask = Column(Integer)
        high = Column(Integer)
        low = Column(Integer)
        volume = Column(BigInteger)
        timestamp = Column(BigInteger)
        avg = Column(Integer)
        vwap = Column(Integer)

        def __init__(self, *args, **kwargs):
            logging.debug('bitcurex.btcpln.ticker, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<last=%s, bid=%s, ask=%s, high=%s, '
            'low=%s, volume=%s, timestamp=%s, avg=%s, vwap=%s>' % \
                (self.last, self.bid, self.ask, self.high, self.low,
                    self.volume, self.timestamp, self.avg, self.vwap)

    class tradelist(Base):
        """
        {"price": "1885.00000000", "amount": "0.25000000", "type": 1,
        "date": 1395197708, "tid": 292860}
        """
        __tablename__ = "bitcurex_btcpln_tradelist"
        __mapper_args__ = {
            'polymorphic_identity': __tablename__
        }

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer)  # buy: 1, sell: 2
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>'\
                % (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class BTCEUR():

    class ticker(Base):
        """
        {"high": 444.9, "low": 400, "avg": 422.45, "vwap": 418.08122135,
        "vol": 12.15797142, "last": 418, "buy": 418, "sell": 429.99,
        "time": 1395338167}
        """

        __tablename__ = "bitcurex_btceur_ticker"

        id = Column(Integer, primary_key=True)
        last = Column(Integer)
        bid = Column(Integer)
        ask = Column(Integer)
        high = Column(Integer)
        low = Column(Integer)
        volume = Column(BigInteger)
        timestamp = Column(Integer)
        avg = Column(Integer)
        vwap = Column(Integer)

        def __init__(self, *args, **kwargs):
            logging.debug('bitcurex.btcpln.ticker, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<last=%s, bid=%s, ask=%s, high=%s, '
            'low=%s, volume=%s, timestamp=%s, avg=%s, vwap=%s>' % \
                (self.last, self.bid, self.ask, self.high,
                    self.low, self.volume, self.timestamp, self.avg, self.vwap)

    class tradelist(Base):
        """
        {"price": "418.00000000", "amount": "0.93837438", "type": 1,
        "date": 1395337880, "tid": 18754}
        """
        __tablename__ = "bitcurex_btceur_tradelist"
        __mapper_args__ = {
            'polymorphic_identity': __tablename__
        }

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer)  # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>'\
                % (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)
