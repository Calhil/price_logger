from sqlalchemy import Column, Integer, BigInteger, SmallInteger

from base import Base


class ABYBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_abybtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class ACBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_acbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class ADNBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_adnbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class AIRBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_airbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class AURBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_aurbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class BANKBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_bankbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class BCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_bcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class BCCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_bccbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class BCNBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_bcnbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class BDCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_bdcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class BDGBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_bdgbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class BELABTC():

    class tradelist(Base):
        __tablename__ = "poloniex_belabtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class BITSBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_bitsbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class BLUBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_blubtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class BNSBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_bnsbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class BONESBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_bonesbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class CACHBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_cachbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class CCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_ccbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class CGABTC():

    class tradelist(Base):
        __tablename__ = "poloniex_cgabtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class CHABTC():

    class tradelist(Base):
        __tablename__ = "poloniex_chabtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class CINNIBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_cinnibtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class CNOTEBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_cnotebtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class COMMBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_commbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class CUREBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_curebtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class DIEMBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_diembtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class DOGEBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_dogebtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class DRKBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_drkbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class DRMBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_drmbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class EBTBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_ebtbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class EFLBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_eflbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class EMC2BTC():

    class tradelist(Base):
        __tablename__ = "poloniex_emc2btc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class EXEBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_exebtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class FLTBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_fltbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class GDNBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_gdnbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class GIARBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_giarbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class GLBBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_glbbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class GOLDBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_goldbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class GRCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_grcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class GRSBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_grsbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class HIROBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_hirobtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class HOTBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_hotbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class HUCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_hucbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class ITCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_itcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class IXCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_ixcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class JUGBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_jugbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class LCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_lcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class LCLBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_lclbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class LGCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_lgcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class LTCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_ltcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class METHBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_methbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class MINTBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_mintbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class MMCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_mmcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class MNS1BTC():

    class tradelist(Base):
        __tablename__ = "poloniex_mns1btc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class MONBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_monbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class MRCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_mrcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class MROBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_mrobtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class MUNBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_munbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class MYRBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_myrbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class NASBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_nasbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class NAUTBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_nautbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class NC2BTC():

    class tradelist(Base):
        __tablename__ = "poloniex_nc2btc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class NMCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_nmcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class NOBLBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_noblbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class NOTEBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_notebtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class NRSBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_nrsbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class NXTBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_nxtbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class PAWNBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_pawnbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class PIGBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_pigbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class PLXBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_plxbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class PMCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_pmcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class PPCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_ppcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class PRCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_prcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class PRTBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_prtbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class PTSBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_ptsbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class Q2CBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_q2cbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class QCNBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_qcnbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class REDDBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_reddbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class RICBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_ricbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class SCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_scbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class SHIBEBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_shibebtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class SOCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_socbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class SRCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_srcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class SYNCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_syncbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class USDEBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_usdebtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class UVCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_uvcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class VRCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_vrcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class VTCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_vtcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class WCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_wcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class WDCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_wdcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class WOLFBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_wolfbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class XBCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_xbcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class XCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_xcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class XCPBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_xcpbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class XLBBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_xlbbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class XPMBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_xpmbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class XSIBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_xsibtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class XSVBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_xsvbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class YACCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_yaccbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class YANGBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_yangbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class YCBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_ycbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)


class YINBTC():

    class tradelist(Base):
        __tablename__ = "poloniex_yinbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price={0}, amount={1}, tid={2}, timestamp={3}, operation={4}>'\
                   .format(self.price, self.amount, self.tid, self.timestamp, self.operation)
