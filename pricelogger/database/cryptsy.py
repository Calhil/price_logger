import logging
from sqlalchemy import Column, Integer, BigInteger

from base import Base


#class cryptsy():


#    class LTCUSD():
#
#        class tradelist(Base):
#
#            __tablename__ = "cryptsy_ltcusd_tradelist"
#
#            id = Column(Integer, primary_key=True)
#            price = Column(BigInteger)
#            amount = Column(BigInteger)
#            amount2 = Column(BigInteger) # price*amount, total btc
#            operation = Column(Integer) # buy: 1, sell: 2
#            tid = Column(Integer)
#            timestamp = Column(Integer)
#
#            def __init__(self, *args, **kwargs):
#                #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
#                for name, value in kwargs.items():
#                    setattr(self, name,value)
#
#            def __repr__(self):
#                return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
#                    self.price, self.amount, self.operation, self.tid, self.timestamp)
#
#
#    class BTCUSD():
#
#        class tradelist(Base):
#
#            __tablename__ = "cryptsy_btcusd_tradelist"
#
#            id = Column(Integer, primary_key=True)
#            price = Column(BigInteger)
#            amount = Column(BigInteger)
#            amount2 = Column(BigInteger) # price*amount, total btc
#            operation = Column(Integer) # buy: 1, sell: 2
#            tid = Column(Integer)
#            timestamp = Column(Integer)
#
#            def __init__(self, *args, **kwargs):
#                #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
#                for name, value in kwargs.items():
#                    setattr(self, name,value)
#
#            def __repr__(self):
#                return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
#                    self.price, self.amount, self.operation, self.tid, self.timestamp)



class LTCBTC():

    class tradelist(Base):
        """
        {u'datetime': u'2014-05-02 18:39:59',
        u'initiate_ordertype': u'Sell',
        u'quantity': u'0.01000000',
        u'total': u'0.00023807',
        u'tradeid': u'42214684',
        u'tradeprice': u'0.02380712'}
        """
        __tablename__ = "cryptsy_ltcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)


        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class FTCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ftcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


#    class FTCUSD():
#
#        class tradelist(Base):
#
#            __tablename__ = "cryptsy_ftcusd_tradelist"
#
#            id = Column(Integer, primary_key=True)
#            price = Column(BigInteger)
#            amount = Column(BigInteger)
#            amount2 = Column(BigInteger) # price*amount, total btc
#            operation = Column(Integer) # buy: 1, sell: 2
#            tid = Column(Integer)
#            timestamp = Column(Integer)
#
#            def __init__(self, *args, **kwargs):
#                #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
#                for name, value in kwargs.items():
#                    setattr(self, name,value)
#
#            def __repr__(self):
#                return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
#                    self.price, self.amount, self.operation, self.tid, self.timestamp)


class MNCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_mncbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CNCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_cncbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class RYCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_rycbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class BQCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_bqcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class YACBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_yacbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ELCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_elcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class NVCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_nvcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class WDCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_wdcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CNCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_cncltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class WDCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_wdcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class YACLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_yacltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class BTBBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_btbbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class JKCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_jkcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class DGCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_dgcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class TRCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_trcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class PPCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ppcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class NMCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_nmcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class GLDBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_gldbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class PXCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_pxcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class NBLBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_nblbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class FRKBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_frkbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class LKYBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_lkybtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class JKCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_jkcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class GLDLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_gldltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class RYCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_rycltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class IXCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ixcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class FRCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_frcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class DVCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_dvcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class AMCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_amcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class FSTBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_fstbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class MECBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_mecbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class DBLLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_dblltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class EZCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ezcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ARGBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_argbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class BTEBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_btebtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class BTGBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_btgbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class SBCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_sbcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class DVCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_dvcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CAPBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_capbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class NRBBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_nrbbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class EZCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ezcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class MEMLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_memltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ALFBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_alfbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CRCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_crcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class IFCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ifcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class IFCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ifcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class FLOLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_floltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class MSTLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_mstltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class XPMBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_xpmbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class NANBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_nanbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class KGCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_kgcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ANCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ancbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class XNCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_xncltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CSCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_cscbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class EMDBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_emdbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CGBBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_cgbbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class QRKBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_qrkbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class DMDBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_dmdbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class YBCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ybcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CMCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_cmcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ORBBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_orbbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class GLCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_glcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class GLXBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_glxbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class HBNBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_hbnbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class SPTBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_sptbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class GDCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_gdcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class STRBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_strbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class GMELTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_gmeltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ZETBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_zetbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class PHSBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_phsbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class REDLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_redltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class SRCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_srcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class NECBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_necbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CPRLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_cprltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class PYCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_pycbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ELPLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_elpltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ADTLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_adtltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CLRBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_clrbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class DGCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_dgcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class SXCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_sxcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class MECLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_mecltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class PXCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_pxcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class BUKBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_bukbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class XPMLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_xpmltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class TIXLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_tixltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class NETLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_netltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class COLLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_colltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ASCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ascltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class TEKBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_tekbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class XJOBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_xjobtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class LK7BTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_lk7btc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class TAGBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_tagbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class PTSBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ptsbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class PointsBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_pointsbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ANCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ancltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CGBLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_cgbltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class FSTLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_fstltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class PPCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ppcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class QRKLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_qrkltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ZETLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_zetltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class SBCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_sbcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class BETBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_betbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class TGCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_tgcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class DEMBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_dembtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class DOGEBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_dogebtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class UNOBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_unobtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class NETBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_netbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class DOGELTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_dogeltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CATBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_catbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class LOTBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_lotbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class FFCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ffcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class EACBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_eacbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ZCCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_zccbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


#    class 42BTC():
#
#        class tradelist(Base):
#
#            __tablename__ = "cryptsy_42btc_tradelist"
#
#            id = Column(Integer, primary_key=True)
#            price = Column(BigInteger)
#            amount = Column(BigInteger)
#            amount2 = Column(BigInteger) # price*amount, total btc
#            operation = Column(Integer) # buy: 1, sell: 2
#            tid = Column(Integer)
#            timestamp = Column(Integer)
#
#            def __init__(self, *args, **kwargs):
#                #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
#                for name, value in kwargs.items():
#                    setattr(self, name,value)
#
#            def __repr__(self):
#                return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
#                    self.price, self.amount, self.operation, self.tid, self.timestamp)


class BCXBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_bcxbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class RPCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_rpcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class OSCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_oscbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class MOONLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_moonltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class TIPSLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_tipsltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class LEAFBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_leafbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class MEOWBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_meowbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CASHBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_cashbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)



class VTCBTC():

    class tradelist(Base):
        """
        """
        __tablename__ = "cryptsy_vtcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class MAXBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_maxbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class SXCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_sxcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CACHBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_cachbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class DRKBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_drkbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class MINTBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_mintbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class BENBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_benbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class SMCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_smcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class NXTBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_nxtbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class AURBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_aurbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class AURLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_aurltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class NXTLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_nxtltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class UTCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_utcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class MZCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_mzcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class FLAPBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_flapbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class TAKBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_takbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class DGBBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_dgbbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class SATBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_satbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class RDDBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_rddbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ZEDBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_zedbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class FRKLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_frkltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class POTBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_potbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CTMLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_ctmltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ZEITLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_zeitltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class LYCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_lycbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class KDCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_kdcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class BCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_bcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class SPABTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_spabtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


#    class DOGEUSD():
#
#        class tradelist(Base):
#
#            __tablename__ = "cryptsy_dogeusd_tradelist"
#
#            id = Column(Integer, primary_key=True)
#            price = Column(BigInteger)
#            amount = Column(BigInteger)
#            amount2 = Column(BigInteger) # price*amount, total btc
#            operation = Column(Integer) # buy: 1, sell: 2
#            tid = Column(Integer)
#            timestamp = Column(Integer)
#
#            def __init__(self, *args, **kwargs):
#                #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
#                for name, value in kwargs.items():
#                    setattr(self, name,value)
#
#            def __repr__(self):
#                return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
#                    self.price, self.amount, self.operation, self.tid, self.timestamp)


class EXEBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_exebtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class NYANBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_nyanbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class HVCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_hvcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class BATLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_batltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class MN1BTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_mn1btc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class EMC2BTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_emc2btc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class MRYBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_mrybtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class RBBTLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_rbbtltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class BCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_bcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class FLTBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_fltbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class KARMLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_karmltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class DMCLTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_dmcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class WCBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_wcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class MN2BTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_mn2btc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class CINNIBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_cinnibtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class COMMBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_commbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)


class ACBTC():

    class tradelist(Base):

        __tablename__ = "cryptsy_acbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        amount2 = Column(BigInteger) # price*amount, total btc
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={0}, amount={1}, operation={2}, tid={3}, timestamp={4}>'.format(
                self.price, self.amount, self.operation, self.tid, self.timestamp)
