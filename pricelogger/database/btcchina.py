from sqlalchemy import Column, Integer, BigInteger, SmallInteger

from base import Base


class BTCCNY():

    class tradelist(Base):
        """
        """
        __tablename__ = "btcchina_btccny_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
                   'operation=%s>' % (
                       self.price, self.amount,
                       self.tid, self.timestamp, self.operation)

class LTCCNY():

    class tradelist(Base):
        """
        """
        __tablename__ = "btcchina_ltccny_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
                   'operation=%s>' % (
                       self.price, self.amount,
                       self.tid, self.timestamp, self.operation)

class LTCBTC():

    class tradelist(Base):
        """
        """
        __tablename__ = "btcchina_ltcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
                   'operation=%s>' % (
                       self.price, self.amount,
                       self.tid, self.timestamp, self.operation)

