import logging
from sqlalchemy import Column, Integer, BigInteger

from base import Base


#class btce():
class BTCUSD():
    class ticker(Base):
        """
        {"ltc_btc":{"high":0.03384,"low":0.02838,"avg":0.03111,"vol":14345.52527,"vol_cur":468078.98962,"last":0.02902,"buy":0.02902,"sell":0.02898,"updated":1395266435}}
        """

        __tablename__ = "btce_btcusd_ticker"

        id = Column(Integer, primary_key=True)
        last = Column(Integer)
        bid = Column(Integer)
        ask = Column(Integer)
        high = Column(Integer)
        low = Column(Integer)
        volume = Column(BigInteger)
        volume2 = Column(BigInteger)
        timestamp = Column(Integer)
        avg = Column(Integer)

        def __init__(self, *args, **kwargs):
            logging.debug('btce.btcusd.ticker, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<last=%s, bid=%s, ask=%s, high=%s, low=%s, ' \
            'volume=%s, timestamp=%s, avg=%s, volume2=%s>' % \
                        (self.last, self.bid, self.ask, self.high, self.low,
                            self.volume, self.timestamp, self.avg, self.volume2)


    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_btcusd_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, ' \
            'tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class LTCBTC():
    class ticker(Base):

        __tablename__ = "btce_ltcbtc_ticker"

        id = Column(Integer, primary_key=True)
        last = Column(Integer)
        bid = Column(Integer)
        ask = Column(Integer)
        high = Column(Integer)
        low = Column(Integer)
        volume = Column(BigInteger)
        volume2 = Column(BigInteger)
        timestamp = Column(Integer)
        avg = Column(Integer)

        def __init__(self, *args, **kwargs):
            logging.debug('btce.LTCBTC.ticker, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<last=%s, bid=%s, ask=%s, high=%s, low=%s, ' \
            'volume=%s, timestamp=%s, avg=%s, volume2=%s>' % \
                        (self.last, self.bid, self.ask, self.high, self.low,
                            self.volume, self.timestamp, self.avg, self.volume2)


    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_ltcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, ' \
            'tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)



class PPCBTC():
    class ticker(Base):

        __tablename__ = "btce_ppcbtc_ticker"

        id = Column(Integer, primary_key=True)
        last = Column(Integer)
        bid = Column(Integer)
        ask = Column(Integer)
        high = Column(Integer)
        low = Column(Integer)
        volume = Column(BigInteger)
        volume2 = Column(BigInteger)
        timestamp = Column(Integer)
        avg = Column(Integer)

        def __init__(self, *args, **kwargs):
            logging.debug('btce.ppcbtc.ticker, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<last=%s, bid=%s, ask=%s, high=%s, low=%s, ' \
            'volume=%s, timestamp=%s, avg=%s, volume2=%s>' % \
                        (self.last, self.bid, self.ask, self.high, self.low,
                            self.volume, self.timestamp, self.avg, self.volume2)


    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_ppcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, ' \
            'tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class FTCBTC():
    class ticker(Base):

        __tablename__ = "btce_ftcbtc_ticker"

        id = Column(Integer, primary_key=True)
        last = Column(Integer)
        bid = Column(Integer)
        ask = Column(Integer)
        high = Column(Integer)
        low = Column(Integer)
        volume = Column(BigInteger)
        volume2 = Column(BigInteger)
        timestamp = Column(Integer)
        avg = Column(Integer)

        def __init__(self, *args, **kwargs):
            logging.debug('btce.ftcbtc.ticker, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<last=%s, bid=%s, ask=%s, high=%s, low=%s, ' \
            'volume=%s, timestamp=%s, avg=%s, volume2=%s>' % \
                        (self.last, self.bid, self.ask, self.high, self.low,
                            self.volume, self.timestamp, self.avg, self.volume2)


    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_ftcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, ' \
            'tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class NMCBTC():
    class ticker(Base):

        __tablename__ = "btce_nmcbtc_ticker"

        id = Column(Integer, primary_key=True)
        last = Column(Integer)
        bid = Column(Integer)
        ask = Column(Integer)
        high = Column(Integer)
        low = Column(Integer)
        volume = Column(BigInteger)
        volume2 = Column(BigInteger)
        timestamp = Column(Integer)
        avg = Column(Integer)

        def __init__(self, *args, **kwargs):
            logging.debug('btce.nmcbtc.ticker, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<last=%s, bid=%s, ask=%s, high=%s, low=%s, ' \
            'volume=%s, timestamp=%s, avg=%s, volume2=%s>' % \
                        (self.last, self.bid, self.ask, self.high, self.low,
                            self.volume, self.timestamp, self.avg, self.volume2)


    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_nmcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, ' \
            'tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class LTCUSD():

    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_ltcusd_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, timestamp={}>'\
                .format(self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class BTCCNH():

    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_btccnh_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, timestamp={}>'\
                .format(self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class BTCEUR():

    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_btceur_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, timestamp={}>'\
                .format(self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class BTCGBP():

    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_btcgbp_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, timestamp={}>'\
                .format(self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class LTCCNH():

    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_ltccnh_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, timestamp={}>'\
                .format(self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class LTCEUR():

    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_ltceur_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, timestamp={}>'\
                .format(self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class LTCGBP():

    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_ltcgbp_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, timestamp={}>'\
                .format(self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class LTCRUR():

    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_ltcrur_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, timestamp={}>'\
                .format(self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class NMCUSD():

    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_nmcusd_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, timestamp={}>'\
                .format(self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class PPCUSD():

    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_ppcusd_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, timestamp={}>'\
                .format(self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class XPMBTC():

    class tradelist(Base):
        """
        {u'tid': 32963670, u'timestamp': 1395347601, u'price': 0.00454, u'type': u'bid', u'amount': 99.7956}
        """
        __tablename__ = "btce_xpmbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(Integer) # buy: 1, sell: 2
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price={}, amount={}, operation={}, tid={}, timestamp={}>'\
                .format(self.price, self.amount, self.operation,
                    self.tid, self.timestamp)

