import logging
from sqlalchemy import Column, Integer, BigInteger, SmallInteger

from base import Base

#class bter():

class LTCBTC():

    class tradelist(Base):
        """
        """

        __tablename__ = "bter_ltcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)


        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class DOGEBTC():

    class tradelist(Base):
        """
        """

        __tablename__ = "bter_dogebtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)


        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class VTCBTC():

    class tradelist(Base):
        """
        """

        __tablename__ = "bter_vtcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)


        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)



class KDCBTC():

    class tradelist(Base):
        """
        """

        __tablename__ = "bter_kdcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)


        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)



class QRKBTC():

    class tradelist(Base):
        """
        """

        __tablename__ = "bter_qrkbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(Integer)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)


        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class BTCCNY():

    class tradelist(Base):
        __tablename__ = "bter_btccny_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class LTCCNY():

    class tradelist(Base):
        __tablename__ = "bter_ltccny_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class DOGECNY():

    class tradelist(Base):
        __tablename__ = "bter_dogecny_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class VTCCNY():

    class tradelist(Base):
        __tablename__ = "bter_vtccny_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class ACBTC():

    class tradelist(Base):
        __tablename__ = "bter_acbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class AURBTC():

    class tradelist(Base):
        __tablename__ = "bter_aurbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class BCBTC():

    class tradelist(Base):
        __tablename__ = "bter_bcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class BQCBTC():

    class tradelist(Base):
        __tablename__ = "bter_bqcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class BTBBTC():

    class tradelist(Base):
        __tablename__ = "bter_btbbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class BUKBTC():

    class tradelist(Base):
        __tablename__ = "bter_bukbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class C2BTC():

    class tradelist(Base):
        __tablename__ = "bter_c2btc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class CDCBTC():

    class tradelist(Base):
        __tablename__ = "bter_cdcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class COMMBTC():

    class tradelist(Base):
        __tablename__ = "bter_commbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class CMCBTC():

    class tradelist(Base):
        __tablename__ = "bter_cmcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class CNCBTC():

    class tradelist(Base):
        __tablename__ = "bter_cncbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class DGCBTC():

    class tradelist(Base):
        __tablename__ = "bter_dgcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class DRKBTC():

    class tradelist(Base):
        __tablename__ = "bter_drkbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class DTCBTC():

    class tradelist(Base):
        __tablename__ = "bter_dtcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class EXCBTC():

    class tradelist(Base):
        __tablename__ = "bter_excbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class FLTBTC():

    class tradelist(Base):
        __tablename__ = "bter_fltbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class FRCBTC():

    class tradelist(Base):
        __tablename__ = "bter_frcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class FTCBTC():

    class tradelist(Base):
        __tablename__ = "bter_ftcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class MAXBTC():

    class tradelist(Base):
        __tablename__ = "bter_maxbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class MECBTC():

    class tradelist(Base):
        __tablename__ = "bter_mecbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class MINTBTC():

    class tradelist(Base):
        __tablename__ = "bter_mintbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class MMCBTC():

    class tradelist(Base):
        __tablename__ = "bter_mmcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class NECBTC():

    class tradelist(Base):
        __tablename__ = "bter_necbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class NMCBTC():

    class tradelist(Base):
        __tablename__ = "bter_nmcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class NXTBTC():

    class tradelist(Base):
        __tablename__ = "bter_nxtbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class PPCBTC():

    class tradelist(Base):
        __tablename__ = "bter_ppcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class PRTBTC():

    class tradelist(Base):
        __tablename__ = "bter_prtbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class PTSBTC():

    class tradelist(Base):
        __tablename__ = "bter_ptsbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class SRCBTC():

    class tradelist(Base):
        __tablename__ = "bter_srcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class TAGBTC():

    class tradelist(Base):
        __tablename__ = "bter_tagbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class YACBTC():

    class tradelist(Base):
        __tablename__ = "bter_yacbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class WDCBTC():

    class tradelist(Base):
        __tablename__ = "bter_wdcbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class XCPBTC():

    class tradelist(Base):
        __tablename__ = "bter_xcpbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class XPMBTC():

    class tradelist(Base):
        __tablename__ = "bter_xpmbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class ZCCBTC():

    class tradelist(Base):
        __tablename__ = "bter_zccbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class ZETBTC():

    class tradelist(Base):
        __tablename__ = "bter_zetbtc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class CENTLTC():

    class tradelist(Base):
        __tablename__ = "bter_centltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class DVCLTC():

    class tradelist(Base):
        __tablename__ = "bter_dvcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class IFCLTC():

    class tradelist(Base):
        __tablename__ = "bter_ifcltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class NETLTC():

    class tradelist(Base):
        __tablename__ = "bter_netltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class REDLTC():

    class tradelist(Base):
        __tablename__ = "bter_redltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class TIPSLTC():

    class tradelist(Base):
        __tablename__ = "bter_tipsltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)


class TIXLTC():

    class tradelist(Base):
        __tablename__ = "bter_tixltc_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)
        operation = Column(SmallInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name,value)

        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s, ' \
            'operation=%s>' % (self.price, self.amount,
                    self.tid, self.timestamp, self.operation)
