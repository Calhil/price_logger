from sqlalchemy import Column, Integer, BigInteger, SmallInteger

from base import Base


class ACBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_acbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class AURBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_aurbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class BCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_bcbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class BCLTC():

    class tradelist(Base):
        __tablename__ = "mintpal_bcltc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class BTCSBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_btcsbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class C2BTC():

    class tradelist(Base):
        __tablename__ = "mintpal_c2btc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class CAIxBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_caixbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class CINNIBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_cinnibtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class CLOAKBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_cloakbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class COMMBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_commbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class CRYPTBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_cryptbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class DGBBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_dgbbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class DOGEBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_dogebtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class DOPEBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_dopebtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class DRKBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_drkbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class DRKLTC():

    class tradelist(Base):
        __tablename__ = "mintpal_drkltc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class ECCLTC():

    class tradelist(Base):
        __tablename__ = "mintpal_eccltc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class EMC2BTC():

    class tradelist(Base):
        __tablename__ = "mintpal_emc2btc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class EMOLTC():

    class tradelist(Base):
        __tablename__ = "mintpal_emoltc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class ENCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_encbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class FACBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_facbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class FLTBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_fltbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class GRSBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_grsbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class GRUMPLTC():

    class tradelist(Base):
        __tablename__ = "mintpal_grumpltc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class HIROBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_hirobtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class HVCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_hvcbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class ITCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_itcbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class IVCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_ivcbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class KARMLTC():

    class tradelist(Base):
        __tablename__ = "mintpal_karmltc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class KDCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_kdcbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class LTCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_ltcbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class METHBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_methbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class MINTBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_mintbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class MRCLTC():

    class tradelist(Base):
        __tablename__ = "mintpal_mrcltc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class MRSBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_mrsbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class MYCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_mycbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class MYRBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_myrbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class MZCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_mzcbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class NAUTBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_nautbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class NOBLBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_noblbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class OLYBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_olybtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class PANDALTC():

    class tradelist(Base):
        __tablename__ = "mintpal_pandaltc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class PENGLTC():

    class tradelist(Base):
        __tablename__ = "mintpal_pengltc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class PNDBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_pndbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class PNDLTC():

    class tradelist(Base):
        __tablename__ = "mintpal_pndltc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class POTBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_potbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class RICBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_ricbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class RZRBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_rzrbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class SATBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_satbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class SCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_scbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class SPABTC():

    class tradelist(Base):
        __tablename__ = "mintpal_spabtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class SUPERBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_superbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class SYNCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_syncbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class TAKBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_takbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class TESBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_tesbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class TOPLTC():

    class tradelist(Base):
        __tablename__ = "mintpal_topltc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class UNOBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_unobtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class USDeBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_usdebtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class UTCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_utcbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class VRCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_vrcbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class VTCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_vtcbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class WCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_wcbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class XCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_xcbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class XLBBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_xlbbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class XMRBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_xmrbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class YCBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_ycbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class ZEDBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_zedbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class ZEITLTC():

    class tradelist(Base):
        __tablename__ = "mintpal_zeitltc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class ZETBTC():

    class tradelist(Base):
        __tablename__ = "mintpal_zetbtc_tradelist"
        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)
