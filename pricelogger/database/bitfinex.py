from sqlalchemy import Column, Integer, BigInteger, SmallInteger

from base import Base


class BTCUSD():

    class tradelist(Base):
        """
        { "timestamp": 1402525110,
        "tid": 2146657,
        "price": "640.4",
        "amount": "1.0",
        "exchange": "bitfinex",
        "type": "sell"}
        """
        __tablename__ = "bitfinex_btcusd_tradelist"
        __mapper_args__ = {
            'polymorphic_identity': __tablename__
        }

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)  # buy: 1, sell: 2
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>'\
                % (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class LTCUSD():

    class tradelist(Base):
        """
        { "timestamp": 1402525110,
        "tid": 2146657,
        "price": "640.4",
        "amount": "1.0",
        "exchange": "bitfinex",
        "type": "sell"}
        """
        __tablename__ = "bitfinex_ltcusd_tradelist"
        __mapper_args__ = {
            'polymorphic_identity': __tablename__
        }

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)  # buy: 1, sell: 2
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>'\
                % (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class DRKUSD():

    class tradelist(Base):
        """
        { "timestamp": 1402525110,
        "tid": 2146657,
        "price": "640.4",
        "amount": "1.0",
        "exchange": "bitfinex",
        "type": "sell"}
        """
        __tablename__ = "bitfinex_drkusd_tradelist"
        __mapper_args__ = {
            'polymorphic_identity': __tablename__
        }

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)  # buy: 1, sell: 2
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>'\
                % (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class DRKBTC():

    class tradelist(Base):
        """
        { "timestamp": 1402525110,
        "tid": 2146657,
        "price": "640.4",
        "amount": "1.0",
        "exchange": "bitfinex",
        "type": "sell"}
        """
        __tablename__ = "bitfinex_drkbtc_tradelist"
        __mapper_args__ = {
            'polymorphic_identity': __tablename__
        }

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)  # buy: 1, sell: 2
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>'\
                % (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)


class LTCBTC():

    class tradelist(Base):
        """
        { "timestamp": 1402525110,
        "tid": 2146657,
        "price": "640.4",
        "amount": "1.0",
        "exchange": "bitfinex",
        "type": "sell"}
        """
        __tablename__ = "bitfinex_ltcbtc_tradelist"
        __mapper_args__ = {
            'polymorphic_identity': __tablename__
        }

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        operation = Column(SmallInteger)  # buy: 1, sell: 2
        tid = Column(BigInteger)
        timestamp = Column(BigInteger)

        def __init__(self, *args, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __repr__(self):
            return '<price=%s, amount=%s, operation=%s, tid=%s, timestamp=%s>'\
                % (self.price, self.amount, self.operation,
                    self.tid, self.timestamp)
