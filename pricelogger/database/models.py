from sqlalchemy import Column, String, Integer, BigInteger

from base import Base
#from . import Base


class SchedulerTable(Base):
    """ table used to store timestamps used to schedule api calls """

    #exchange.pair.apimethod timestamp
    __tablename__ = "scheduler_table"

    id = Column(Integer, primary_key=True)
    itemname = Column(String(40))
    timestamp = Column(BigInteger)

    def __init__(self, itemname, timestamp):
        self.itemname = itemname
        self.timestamp = timestamp

    def __repr__(self):
        return 'itemname={0}, timestamp={1}'.format(self.itemname, self.timestamp)
