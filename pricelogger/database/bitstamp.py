import logging
from sqlalchemy import Column, Integer, BigInteger

from base import Base


#class bitstamp():

class BTCUSD():

    class ticker(Base):

        __tablename__ = "bitstamp_btcusd_ticker"

        id = Column(Integer, primary_key=True)
        last = Column(Integer)
        bid = Column(Integer)
        ask = Column(Integer)
        high = Column(Integer)
        low = Column(Integer)
        volume = Column(BigInteger)
        timestamp = Column(Integer)
        vwap = Column(Integer)

        def __init__(self, **kwargs):
            logging.debug('bitstamp.BTCUSD.ticker, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name, value)


        def __repr__(self):
            return '<last=%s, bid=%s, ask=%s, high=%s, '
            'low=%s, volume=%s, timestamp=%s, vwap=%s>' % \
                (self.last, self.bid, self.ask, self.high,
                    self.low, self.volume, self.timestamp, self.vwap)


    class tradelist(Base):
        """
            {"date": "1395093627", "tid": 4068697, "price": "624.08", "amount": "0.00165000"}
        """
        __tablename__ = "bitstamp_btcusd_tradelist"

        id = Column(Integer, primary_key=True)
        price = Column(BigInteger)
        amount = Column(BigInteger)
        tid = Column(Integer)
        timestamp = Column(Integer)

        def __init__(self, *args, **kwargs):
            #logging.debug('bitcurex.btcpln.tradelist, kwargs=%s)' % kwargs)
            for name, value in kwargs.items():
                setattr(self, name,value)


        def __repr__(self):
            return '<price=%s, amount=%s, tid=%s, timestamp=%s>' % \
                (self.price, self.amount,
                    self.tid, self.timestamp)



