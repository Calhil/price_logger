import multiprocessing
import logging
import timeit
from operator import itemgetter
from time import time, sleep
from datetime import timedelta
#from sqlalchemy.orm.exc import NoResultFound

from database import Session
from config import MAX_FETCH__DELAY
from config import EXCHANGES_ALLOWED
from api import exchanges
from database.models import SchedulerTable

# this is needed for eval function to work
from database import *


class Fetcher(multiprocessing.Process):

    def __init__(self, Q_in, Q_out, dict_tid):
        multiprocessing.Process.__init__(self)
        logging.info('Initializing {0}'.format(self.name))
        self.q_in = Q_in
        self.q_out = Q_out
        self.d = dict_tid

    def run(self):

        while True:
            try:
                # get an item from queue
                kEx, kPair, kApi, vApi = self.q_in.get()

                tic = timeit.default_timer()
                data = None

                if kApi == 'tradelist' and kApi in EXCHANGES_ALLOWED.keys():
                    #logging.debug('%s.%s.%s' % (kEx, kPair, kApi))

                    # TODO: new method with shared dict
                    lastTid = self.d['{}_{}_{}'.format(kEx, kPair, kApi)]

                    # fetch the data
                    api_class = eval('exchanges.{0}'.format(kEx))
                    api_instance = api_class(
                        url=vApi, currency_pair=kPair, last_transaction_id=lastTid)
                    data = api_instance.gettradelist_data()

                elif kApi == 'ticker' and kApi in EXCHANGES_ALLOWED.keys():
                    #logging.debug('%s.%s.%s' % (kEx, kPair, kApi))

                    api_class = eval('exchanges.{0}'.format(kEx))
                    api_instance = api_class(
                        url=vApi,
                        currency_pair=kPair)
                    data = api_instance.getticker_data()

                    logging.debug('{}:{}.{}.{}: ticker data = {} '.format(
                        self.name,
                        kEx, kPair, kApi,
                        data))

                self.q_in.task_done()

                # send new data for processing
                if data is not None:
                    self.q_out.put([kEx, kPair, kApi, data])

                toc = timeit.default_timer()
                logging.info('{0}: {2}.{3}.{4}: round completed in {1:.2f}s.'.format(
                    self.name, (toc - tic),
                    kEx, kPair, kApi))
            except KeyboardInterrupt:
                    #TODO:
                    logging.warning('{0}: Received '
                                    'shutdown signal.'.format(self.name))
                    break
        return


class Scheduler(multiprocessing.Process):

    def __init__(
            self, Queue_Out, dict_tid, url_array,
            exchanges_allowed={'ticker': [], 'tradelist': [], 'orderbook': []}):
        multiprocessing.Process.__init__(self)
        logging.info('Initializing {0}'.format(self.name))
        self.q_out = Queue_Out
        self.d = dict_tid

        # scheduler variables:
        self.ticktime = 1
        self.urls = url_array
        self.exchanges_allowed = EXCHANGES_ALLOWED
        #print self.exchanges_allowed

    def mainloop(self):
        for kEx, vEx in zip(self.urls.keys(), self.urls.values()):
            # exchange allowed
            if kEx not in self.exchanges_allowed['ticker'] + self.exchanges_allowed['tradelist']:
                continue
            for kPair, vPair in zip(vEx.keys(), vEx.values()):
                for kApi, vApi in zip(vPair.keys(), vPair.values()):
                    # 'tradelist', 'ticker', 'orderlist'

                    #print 'Scheduler: {}.{}.{}'.format(kEx, kPair, kApi)

                    if kEx not in self.exchanges_allowed[kApi]:
                    #if kApi not in self.exchanges_allowed.keys():
                        continue

                    schedule_needed = False
                    load_from_db = False
                    try:
                        # check if its time to run new tasks
                        if vApi['time_scheduled'] <= time():
                            self.q_out.put((kEx, kPair, kApi, vApi['url']))
                            schedule_needed = True
                    except KeyError:

                        # 1st run -> load data from db
                        schedule_needed = True
                        load_from_db = True
                    finally:
                        # schedule next time to run
                        if schedule_needed:
                            session = Session()
                            qry = session.query(SchedulerTable).filter(
                                SchedulerTable.itemname == '{0}.{1}.{2}'
                                .format(kEx, kPair, kApi)).first()

                            timestamp_new = None

                            # application starting, fetch data from db
                            if load_from_db:
                                if qry is not None:
                                    # load fetch time from db
                                    timestamp_new = qry.timestamp
                                else:
                                    # if no database entries exist
                                    # schedule to run the task now
                                    # and create new db entry
                                    logging.info('{0}: Creating new entry in '
                                                 'db for {1}.{2}.{3}'.format(
                                                     self.name, kEx,
                                                     kPair, kApi))
                                    timestamp_new = int(time())
                                    session.add(
                                        SchedulerTable('{0}.{1}.{2}'.format(
                                            kEx, kPair, kApi),
                                            timestamp_new))

                                # get last transaction id
                                if kApi in ['tradelist']:
                                    item_str = '{}_{}_{}'.format(kEx, kPair, kApi)
                                    tradelistclass = eval('%s.%s.%s' % (kEx, kPair, kApi))
                                    qry2 = session.query(tradelistclass).order_by(
                                        tradelistclass.id.desc()).first()
                                    if qry2 is not None:
                                        self.d[item_str] = qry2.tid
                                    else:
                                        self.d[item_str] = 0
                            else:
                                if vApi['fetch_freq'] >= MAX_FETCH__DELAY:
                                    # specified fetch freq is higher than
                                    # desired max delay
                                    timestamp_new = \
                                        int(time()) + MAX_FETCH__DELAY
                                else:
                                    timestamp_new = \
                                        int(time()) + vApi['fetch_freq']

                                #logging.info('{0}: Updating entry in '
                                # 'db for {1}.{2}.{3}'.format(
                                #   self.name, kEx, kPair, kApi))
                                qry.timestamp = timestamp_new

                            self.urls[kEx][kPair][kApi]['time_scheduled'] = \
                                timestamp_new

                            # save timestamp in db
                            Session.commit()
                            session.close()
                            Session.remove()

                            logging.info(
                                '{1}: {2}.{3}.{4} scheduled to run in {0}s.'
                                .format(timedelta(seconds=(int(
                                    self.urls[kEx][kPair][kApi]['time_scheduled']
                                    - time()))), self.name, kEx, kPair, kApi))

    def run(self):
        while True:
            try:
                # moved all the code to a separate function
                self.mainloop()
                sleep(self.ticktime)
            except KeyboardInterrupt:
                logging.warning('{0}: Received shutdown '
                                'signal.'.format(self.name))

                # save timestamps in db
                Session.commit()
                #session.close()
                Session.remove()
                break
        return


class DataProcessor(multiprocessing.Process):

    def __init__(self, Q_in, dict_tid):
        multiprocessing.Process.__init__(self)
        logging.info('Initializing {0}'.format(self.name))
        self.q_in = Q_in
        self.d = dict_tid
        self.lst_tradelist = []
        self.lst_ticker = []

        self.total_transactions = 0
        self.new_transactions = 0

    def savedata(self, list_data):
        if type(list_data) is list:

            # start new db session
            session = Session()

            for i_item in list_data:
                tic = timeit.default_timer()
                # unpack list values
                kEx, kPair, kApi, data = i_item

                lasttid = None
                if kApi == 'tradelist' and kApi in EXCHANGES_ALLOWED.keys():

                    # TODO new method using shared dict
                    # last transactions in db
                    #tradelistClass = eval('%s.%s.%s' % (kEx, kPair, kApi))
                    #lastTransaction = session.\
                    #    query(tradelistClass).order_by(
                    #        tradelistClass.id.desc()).first()
                    lasttid = self.d['{}_{}_{}'.format(
                        kEx, kPair, kApi)]

                    # sort list
                    #logging.info('Sorting data according to tid')
                    try:
                        data = sorted(data, key=itemgetter('tid'))
                    except (KeyError, TypeError):
                        # use timestamp if exchange doesnt issue trade id
                        logging.warning('{}:{}.{}.{}: sorting acording to '
                                        'timestamp'.format(
                                            self.name,
                                            kEx, kPair, kApi))
                        #logging.warning('{}:{}.{}.{}: data: {}'.format(
                        #                    self.name,
                        #                    kEx, kPair, kApi,
                        #                    data))
                        #data = sorted(data, key=itemgetter('timestamp'))
                        # skip this item
                        continue


                    # look for new trade data
                    idx = None
                    for ii in xrange(len(data)):
                        # check if there is new data available
                        if lasttid < data[ii]['tid']:

                            self.total_transactions += len(data) - ii
                            self.new_transactions = len(data) - ii

                            # new last transaction id
                            self.d['{}_{}_{}'.format(kEx, kPair, kApi)] = data[-1]['tid']

                            logging.debug('{0}: Found {1} new transactions for'
                                          ' {2} starting at idx={3}'.format(
                                              self.name, (len(data) - ii),
                                              kEx, ii))
                            logging.debug('{0}: data[{1}] = {2}'.format(
                                self.name, ii, data[ii]))
                            idx = ii
                            break

                    # save the data if new data found
                    if idx is not None:
                        tradelistClass = eval('%s.%s.%s' % (kEx, kPair, kApi))
                        for ii in xrange(idx, len(data)):

                            #logging.info('{0}: {1}.{2}.{3}: <{4}>'.format(
                            #    self.name, kEx, kPair, kApi, data[ii]))

                            newTransaction = tradelistClass(**data[ii])
                            session.add(newTransaction)

                if kApi == 'ticker' and kApi in EXCHANGES_ALLOWED.keys():
                    if data is not None:
                        ticker_class = eval('%s.%s.%s' % (kEx, kPair, kApi))

                        # update number of transactions saved
                        self.total_transactions += 1
                        self.new_transactions = 1

                        session.add(ticker_class(**data))

                toc = timeit.default_timer()
                logging.info('{0}: {3}.{4}.{5} round completed in {1:.2f}s. '
                             'New transactions = {6}/{2}'.format(
                                 self.name, (toc - tic), self.total_transactions,
                                 kEx, kPair, kApi, self.new_transactions))
                self.new_transactions = 0

            tic = timeit.default_timer()
            try:
                session.commit()
            except:
                logging.error('Something went wrong. '
                              'Undoing session changes.')
                session.rollback()
                raise
            session.close()
            Session.remove()

            toc = timeit.default_timer()
            logging.info('{0}: Saving data to db finished in {1:.2f}s.'.format(
                self.name,
                toc - tic))

        return None

    def run(self):
        #self.lst_tradelist = []
        #self.lst_ticker = []
        while True:
            try:
                kEx, kPair, kApi, data = self.q_in.get()

                if False:
                # TODO: remove this after data is downloaded
                    if kEx in ['btcchina', 'okcoin'] and kApi in ['tradelist']:
                        self.savedata([[kEx, kPair, kApi, data]])
                    else:
                        if kApi in ['tradelist']:
                            self.lst_tradelist.append([kEx, kPair, kApi, data])
                        elif kApi in ['ticker']:
                            self.lst_ticker.append([kEx, kPair, kApi, data])
                    self.q_in.task_done()
                    if len(self.lst_tradelist) >= 10:
                        self.savedata(self.lst_tradelist)
                        self.lst_tradelist = []
                    if len(self.lst_ticker) >= 50:
                        self.savedata(self.lst_ticker)
                        self.lst_ticker = []

                self.savedata([[kEx, kPair, kApi, data]])


            except KeyboardInterrupt:
                logging.warning('{0}: Received '
                                'shutdown signal.'.format(self.name))
                self.savedata(self.lst_tradelist)
                #session.rollback()
                #session.close()
                #Session.remove()
                break

        return
