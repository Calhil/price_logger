"""
pricelogger.py
"""
from webdata import getJson
#from config import DELAY
from config import CENT, SATOSHI, EXCHANGES, METHODS

# this is needed for eval function to work
from database import *
from database import Session

#import simplejson as json
from decimal import Decimal
from operator import itemgetter
import logging
from time import sleep, mktime
from tzlocal import get_localzone
from pytz import timezone
from datetime import datetime


# timezones
tz_cryptsy = timezone('US/Eastern')
tz_local = get_localzone()


def choosecoinbase(pair):
    # TODO: probably regex needed here
    # TODO: CNY needs higher precision
    coinbase = None
    if 'USD' in pair or \
            'PLN' in pair or\
            'CNY' in pair or\
            'EUR' in pair:
        coinbase = CENT
    else:
        coinbase = SATOSHI
    logging.debug('Coinbase chosen: {0} for {1}'.format(coinbase, pair))

    return coinbase


def modifyticker(datadir, currencypair):
    """ Applies the correct unit base to ticker values"""

    coinbase = choosecoinbase(currencypair)

    # apply the correct coinbase
    for name, value in datadir.items():
        if name in ('last', 'bid', 'ask', 'high', 'low', 'avg', 'vwap'):
            datadir[name] = int(Decimal(value) * coinbase)
        if name in ('volume', 'volume2'):
            datadir[name] = int(Decimal(value) * SATOSHI)
        if name in ('timestamp'):
            datadir[name] = int(value)

    logging.debug('ModifyTicker:DataDir={0}'.format(datadir))

    return datadir


def fixtickerkeys(data_list):
    """ Applies the correct dictionary keys."""

    if 'buy' in data_list:
        logging.debug('Renaming dictionary keys: buy')
        data_list[u'bid'] = data_list.pop('buy')

    if 'sell' in data_list:
        logging.debug('Renaming dictionary keys: sell')
        data_list[u'ask'] = data_list.pop('sell')

    if 'time' in data_list:
        logging.debug('Renaming dictionary keys: time')
        data_list[u'timestamp'] = data_list.pop('time')

    if 'vol' in data_list:
        logging.debug('Renaming dictionary keys: vol')
        data_list[u'volume'] = data_list.pop('vol')

    if 'vol_cur' in data_list:
        logging.debug('Renaming dictionary keys: vol_cur')
        data_list[u'volume2'] = data_list.pop('vol_cur')

    if 'updated' in data_list:
        logging.debug('Renaming dictionary keys: updated')
        data_list[u'timestamp'] = data_list.pop('updated')

    return data_list


def fixtradelistkeys(data_list):
    """ Applies the correct dictionary keys. """

    logging.debug('Fixing dictionary keys')
    if isinstance(data_list, list):
        for i_data in xrange(len(data_list)):
            # TODO: theres probably a more elegant way to do this
            if 'date' in data_list[i_data]:
                data_list[i_data][u'timestamp'] = data_list[i_data].pop('date')

            if 'datetime' in data_list[i_data]:
                data_list[i_data][u'timestamp'] = \
                    data_list[i_data].pop('datetime')

            if 'type' in data_list[i_data]:
                data_list[i_data][u'operation'] = data_list[i_data].pop('type')

            if 'initiate_ordertype' in data_list[i_data]:
                data_list[i_data][u'operation'] = \
                    data_list[i_data].pop('initiate_ordertype')

            if 'quantity' in data_list[i_data]:
                data_list[i_data]['amount'] = data_list[i_data].pop('quantity')

            if 'total' in data_list[i_data]:
                data_list[i_data]['amount2'] = data_list[i_data].pop('total')

            if 'tradeid' in data_list[i_data]:
                data_list[i_data]['tid'] = int(
                    data_list[i_data].pop('tradeid'))

            if 'tradeprice' in data_list[i_data]:
                data_list[i_data]['price'] = data_list[
                    i_data].pop('tradeprice')

#
#            if '' in r[ii]:
# logging.debug('Renaming dictionary keys: ')
#                r[ii][u''] = r[ii].pop('')

    return data_list


def modifytradelist(data_list, currency_pair, exchange):
    """ Modifies the tradelist values, changes coinbase, time format, etc. """

    coinbase = choosecoinbase(currency_pair)

    # apply the correct coinbase and bid/ask values
    if isinstance(data_list, list):
        for i_data in xrange(len(data_list)):
            for name, value in data_list[i_data].items():
                if name in ('price', 'amount2'):
                    data_list[i_data][name] = int(Decimal(value) * coinbase)
                if name in ('amount'):
                    data_list[i_data][name] = int(Decimal(value) * SATOSHI)
                if name in ('operation') and \
                        exchange in ('btce', 'bter', 'cryptsy'):
                    if value in ('bid', 'buy', 'Buy'):
                        # ask, this is accourding to bitcurex
                        data_list[i_data][name] = 2
                    else:
                        data_list[i_data][name] = 1  # bid

                # exchange specific changes
                if exchange in ('cryptsy'):
                    if name in ('timestamp'):
                        datetime_object = datetime.strptime(
                            value,
                            '%Y-%m-%d %H:%M:%S')
                        data_list[i_data][name] = int(mktime(
                            tz_cryptsy.localize(datetime_object).astimezone(
                                tz_local).timetuple()))

    else:
        logging.warning('ModifyTradelist got wrong variable.'
                        'type = {0}'.format(type(data_list)))

    return data_list


def run(url):
    """ Main loop function """

    session = Session()

    # Main loop
    while True:

        # loop over urls
        for k_ex, v_ex in zip(url.keys(), url.values()):

            if k_ex not in EXCHANGES:
                continue

            for k_pair, v_pair in zip(v_ex.keys(), v_ex.values()):
                for k_api, v_api in zip(v_pair.keys(), v_pair.values()):

                    # ========== ticker ==============
                    if k_api == 'ticker' and k_api in METHODS:
                        logging.debug('%s %s %s' % (k_ex, k_pair, k_api))

                        # this is a class bitstamp.BTCUSD.ticker
                        # ExchangeName.CurrencyPair.ApiFunction
                        tickerClass = eval('%s.%s.%s' % (k_ex, k_pair, k_api))

                        oldTicker = session.query(
                            tickerClass).order_by('-id').first()
                        logging.debug('oldTicker = %s' % (oldTicker))

                        data = ModifyTicker(
                            fixtickerkeys(
                                getJson(
                                    v_api,
                                    exchange=k_ex)),
                            k_pair)
                        logging.debug('data = %s' % (data))

                        newTicker = tickerClass(**data)
                        logging.debug('newTicker = %s' % (newTicker))

                        # check if there is a new ticker available
                        if oldTicker is None or \
                                newTicker.volume != oldTicker.volume:
                            logging.debug('Found new ticker for %s' % (k_ex))

                            session.add(newTicker)
                            session.commit()

                    # ========== tradelist ==============
                    if k_api == 'tradelist' and k_api in METHODS:
                        logging.debug('%s %s %s' % (k_ex, k_pair, k_api))

                        tradelistClass = eval('%s.%s.%s' % (
                            k_ex, k_pair, k_api))

                        lastTransaction = session.\
                            query(tradelistClass).\
                            order_by(-tradelistClass.tid).first()

                        logging.debug('lastTransaction = %s' % lastTransaction)

                        # bter url fix
                        if lastTransaction is not None and k_ex == 'bter':
                            lastTid = lastTransaction.tid
                        else:
                            lastTid = None

                        data = modifytradelist(
                            fixtradelistkeys(
                                getJson(
                                    v_api,
                                    exchange=k_ex,
                                    kApi=k_api,
                                    Tid=lastTid)),
                            CurrencyPair=k_pair,
                            exchange=k_ex)
                        logging.debug('Received %s transactions' % len(data))

                        # sort list
                        # TODO: might be an issue with same timestamp data
                        # FIX: use 'tid' for sorting?
                        logging.debug('Sorting data according to tid')
                        data = sorted(data, key=itemgetter('tid'))

                        idx = None
                        for ii in xrange(len(data)):
                            # check if there is new data available
                            if lastTransaction is None or \
                                    lastTransaction.tid < data[ii]['tid']:
                                logging.debug('Found {0} new transactions for'
                                              '{1} starting at idx={2}'.format(
                                                  ((len(data) - ii), k_ex, ii)))
                                logging.debug('data[%s] = %s' % (ii, data[ii]))

                                idx = ii

                                break

                        logging.debug('idx = %s' % (idx))

                        # save the data if new data found
                        if idx is not None:
                            for ii in xrange(idx, len(data)):
                                newTransaction = tradelistClass(**data[ii])
                                session.add(newTransaction)

                            session.commit()

                    print

        break
        logging.debug('Sleeping for {0}'.format(DELAY))
        sleep(DELAY)
