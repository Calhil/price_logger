"""
Main config file.
"""
import os
import logging

# absolute application path
BASEDIR = os.path.abspath(os.path.dirname(__file__))

# logging
logging.getLogger('').handlers = []
logging.basicConfig(
    format='[%(asctime)s]:%(levelname)s:%(message)s', level=logging.INFO)


# ---------
# database
# ---------
# sqlite
#DATABASE_DIR = 'data'
#DATABASE_FILE = 'database.sqlite'
#ENGINE_ECHO = False  # engine echo
#DATABASE_URL = os.path.join(basedir, DATABASE_DIR, DATABASE_FILE)
#SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DATABASE_URL

# mysql
ENGINE_ECHO = False  # engine echo
#SQLALCHEMY_DATABASE_URI = \
#    'mysql://bartosz:abacadaba@192.168.0.100/pricelogger_db'
SQLALCHEMY_DATABASE_URI = \
    'mysql://pricelogger:pricelogger_password@localhost/pricelogger_db'

# database migration folder
SQLALCHEMY_MIGRATE_REPO = os.path.join(BASEDIR, 'migration_repository')

# -----------------
# CONTROL VARIABLES
# -----------------
# exchanges to monitor

#EXCHANGES_ALLOWED = {
#    "tradelist": ['bitcurex', 'bitmarket'],
#    "ticker": [],
#    'orderbook': []
#}

EXCHANGES_ALLOWED = {
    "tradelist": ['bter',
                  'btce',
                  'btcchina',
                  'bitcurex',
                  'bitstamp',
                  'bitmarket',
                  'cryptsy',
                  'crxzone',
                  'okcoin',
                  'poloniex',
                  'bitfinex'],
    "ticker": ['huobi'],
    'orderbook': []
}


# number of transactions fetched from btc-e api
BTCE_TRADES_LIMIT = '2000'

# retry time between queries
FETCHER_DELAY_ON_ERROR = 1
# how many times fetcher retries queries after anerror occured
FETCHER_MAX_RETRIES = 5
# max time between consecutive fetches of the same coin
MAX_FETCH__DELAY = 1 * 60 * 60

# requests timeout
REQUESTS_TIMEOUT = 15


# exchange api locations
URLS = {
    'huobi': {
        'BTCCNY': {
            'ticker': {
                'url': 'http://market.huobi.com/staticmarket/ticker_btc_json.js',
                'fetch_freq': 10
            }
        },
        'LTCCNY': {
            'ticker': {
                'url': 'http://market.huobi.com/staticmarket/ticker_ltc_json.js',
                'fetch_freq': 10
            }
        }
    },
    'crxzone': {
        'BTCUSD': {
            'tradelist': {
                'url': 'https://www.crxzone.com/API/Trades?currencyPairID=1',
                'fetch_freq': 3600
            }
        },
        'BTCEUR': {
            'tradelist': {
                'url': 'https://www.crxzone.com/API/Trades?currencyPairID=2',
                'fetch_freq': 3600
            }
        },
        'BTCLTC': {
            'tradelist': {
                'url': 'https://www.crxzone.com/API/Trades?currencyPairID=3',
                'fetch_freq': 3600
            }
        },
        'LTCUSD': {
            'tradelist': {
                'url': 'https://www.crxzone.com/API/Trades?currencyPairID=5',
                'fetch_freq': 3600
            }
        },
        'LTCEUR': {
            'tradelist': {
                'url': 'https://www.crxzone.com/API/Trades?currencyPairID=1008',
                'fetch_freq': 3600
            }
        },
        'LTCSGD': {
            'tradelist': {
                'url': 'https://www.crxzone.com/API/Trades?currencyPairID=1009',
                'fetch_freq': 3600
            }
        },
        'BTCSGD': {
            'tradelist': {
                'url': 'https://www.crxzone.com/API/Trades?currencyPairID=1010',
                'fetch_freq': 3600
            }
        }
    },
    'bitcurex': {
        'BTCPLN': {
            'ticker': 'https://pln.bitcurex.com/data/ticker.json',
            'orderbook': 'https://pln.bitcurex.com/data/orderbook.json',
            'tradelist': {
                'url': 'https://bitcurex.com/api/pln/trades.json',
                'fetch_freq': 3600
            }    # trades from last 24h?
        },
        'BTCEUR': {
            'ticker': 'https://eur.bitcurex.com/data/ticker.json',
            'orderbook': 'https://bitcurex.com/api/eur/orderbook.json',
            'tradelist': {
                'url': 'https://bitcurex.com/api/eur/trades.json',
                'fetch_freq': 3600
            }
        }
    },
    'bitfinex': {
        'BTCUSD': {
            'orderbook': 'https://api.bitfinex.com/v1/book/btcusd',
            'tradelist': {
                'url': 'https://api.bitfinex.com/v1/trades/btcusd',
                'fetch_freq': 60
            }
        },
        'LTCUSD': {
            'orderbook': 'https://api.bitfinex.com/v1/book/ltcusd',
            'tradelist': {
                'url': 'https://api.bitfinex.com/v1/trades/ltcusd',
                'fetch_freq': 60
            }
        },
        'LTCBTC': {
            'orderbook': 'https://api.bitfinex.com/v1/book/ltcbtc',
            'tradelist': {
                'url': 'https://api.bitfinex.com/v1/trades/ltcbtc',
                'fetch_freq': 60
            }
        },
        'DRKUSD': {
            'orderbook': 'https://api.bitfinex.com/v1/book/drkusd',
            'tradelist': {
                'url': 'https://api.bitfinex.com/v1/trades/drkusd',
                'fetch_freq': 60
            }
        },
        'DRKBTC': {
            'orderbook': 'https://api.bitfinex.com/v1/book/drkbtc',
            'tradelist': {
                'url': 'https://api.bitfinex.com/v1/trades/drkbtc',
                'fetch_freq': 60
            }
        }
    },
    'bitstamp': {
        'BTCUSD': {
            'ticker': 'https://www.bitstamp.net/api/ticker/',
            'orderbook': 'https://www.bitstamp.net/api/order_book',
            'tradelist': {
                'url': 'https://www.bitstamp.net/api/transactions',
                'fetch_freq': 600
            }    # trades from last hour, can be modified
        }
    },
    'btcchina': {
        'BTCCNY': {
            'tradelist': {
                'url': 'https://data.btcchina.com/data/historydata',
                'fetch_freq': 60
            }
        },
        'LTCCNY': {
            'tradelist': {
                'url': 'https://data.btcchina.com/data/historydata',
                'fetch_freq': 600
            }
        },
        'LTCBTC': {
            'tradelist': {
                'url': 'https://data.btcchina.com/data/historydata',
                'fetch_freq': 600
            }
        }
    },
    'btce': {
        'BTCUSD': {
            'ticker': 'https://btc-e.com/api/3/ticker/btc_usd',
            'orderbook': 'https://btc-e.com/api/3/depth/btc_usd',
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/btc_usd',
                'fetch_freq': 600
            }  # 150 transactions by default, up to 2000 possible
        },
        'LTCBTC': {
            'ticker': 'https://btc-e.com/api/3/ticker/ltc_btc',
            'orderbook': 'https://btc-e.com/api/3/depth/ltc_btc',
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/ltc_btc',
                'fetch_freq': 600
            }  # 150 transactions by default, up to 2000 possible
        },
        'PPCBTC': {
            'ticker': 'https://btc-e.com/api/3/ticker/ppc_btc',
            'orderbook': 'https://btc-e.com/api/3/depth/ppc_btc',
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/ppc_btc',
                'fetch_freq': 600
            }  # 150 transactions by default, up to 2000 possible
        },
        'FTCBTC': {
            'ticker': 'https://btc-e.com/api/3/ticker/ftc_btc',
            'orderbook': 'https://btc-e.com/api/3/depth/ftc_btc',
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/ftc_btc',
                'fetch_freq': 600
            }  # 150 transactions by default, up to 2000 possible
        },
        'NMCBTC': {
            'ticker': 'https://btc-e.com/api/3/ticker/nmc_btc',
            'orderbook': 'https://btc-e.com/api/3/depth/nmc_btc',
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/nmc_btc',
                'fetch_freq': 600
            }  # 150 transactions by default, up to 2000 possible
        },
        'BTCCNH': {
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/btc_cnh',
                'fetch_freq': 600
            }
        },
        'BTCEUR': {
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/btc_eur',
                'fetch_freq': 600
            }
        },
        'BTCGBP': {
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/btc_gbp',
                'fetch_freq': 600
            }
        },
        'LTCCNH': {
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/ltc_cnh',
                'fetch_freq': 600
            }
        },
        'LTCEUR': {
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/ltc_eur',
                'fetch_freq': 600
            }
        },
        'LTCGBP': {
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/ltc_gbp',
                'fetch_freq': 600
            }
        },
        'LTCRUR': {
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/ltc_rur',
                'fetch_freq': 600
            }
        },
        'NMCUSD': {
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/nmc_usd',
                'fetch_freq': 600
            }
        },
        'PPCUSD': {
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/ppc_usd',
                'fetch_freq': 600
            }
        },
        'XPMBTC': {
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/xpm_btc',
                'fetch_freq': 600
            }
        },
        'LTCUSD': {
            'tradelist': {
                'url': 'https://btc-e.com/api/3/trades/ltc_usd',
                'fetch_freq': 600
            }
        }
    },
    'bitmarket': {
        'BTCPLN': {
            'ticker': 'https://www.bitmarket.pl/json/BTCPLN/ticker.json',
            'tradelist': {
                'url': 'https://www.bitmarket.pl/json/BTCPLN/trades.json',
                'fetch_freq': 3600
            }  # transactions from last 1hr
        },
        'LTCPLN': {
            'ticker': 'https://www.bitmarket.pl/json/LTCPLN/ticker.json',
            'tradelist': {
                'url': 'https://www.bitmarket.pl/json/LTCPLN/trades.json',
                'fetch_freq': 3600
            }
        }
    },
    'bter': {
        # http://data.bter.com/api
        'LTCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/ltc_btc',
                'fetch_freq': 3600
            }
        },
        'DOGEBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/doge_btc',
                'fetch_freq': 3600
            }
        },
        'VTCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/vtc_btc',
                'fetch_freq': 7200
            }
        },
        'KDCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/kdc_btc',
                'fetch_freq': 36000
            }
        },
        'QRKBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/qrk_btc',
                'fetch_freq': 7200
            }
        },
        'BTCCNY': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/btc_cny',
                'fetch_freq': 7200
            }
        },
        'LTCCNY': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/ltc_cny',
                'fetch_freq': 7200
            }
        },
        'VTCCNY': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/vtc_cny',
                'fetch_freq': 7200
            }
        },
        'ACBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/ac_btc',
                'fetch_freq': 7200
            }
        },
        'AURBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/aur_btc',
                'fetch_freq': 36000
            }
        },
        'BCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/bc_btc',
                'fetch_freq': 7200
            }
        },
        'BQCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/bqc_btc',
                'fetch_freq': 36000
            }
        },
        'BTBBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/btb_btc',
                'fetch_freq': 36000
            }
        },
        'BUKBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/buk_btc',
                'fetch_freq': 36000
            }
        },
        'C2BTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/c2_btc',
                'fetch_freq': 36000
            }
        },
        'CDCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/cdc_btc',
                'fetch_freq': 36000
            }
        },
        'COMMBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/comm_btc',
                'fetch_freq': 36000
            }
        },
        'CMCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/cmc_btc',
                'fetch_freq': 36000
            }
        },
        'CNCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/cnc_btc',
                'fetch_freq': 36000
            }
        },
        'DGCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/dgc_btc',
                'fetch_freq': 36000
            }
        },
        'DRKBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/drk_btc',
                'fetch_freq': 7200
            }
        },
        'DTCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/dtc_btc',
                'fetch_freq': 36000
            }
        },
        'EXCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/exc_btc',
                'fetch_freq': 36000
            }
        },
        'FLTBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/flt_btc',
                'fetch_freq': 36000
            }
        },
        'FRCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/frc_btc',
                'fetch_freq': 36000
            }
        },
        'FTCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/ftc_btc',
                'fetch_freq': 36000
            }
        },
        'MAXBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/max_btc',
                'fetch_freq': 7200
            }
        },
        'MECBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/mec_btc',
                'fetch_freq': 36000
            }
        },
        'MINTBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/mint_btc',
                'fetch_freq': 36000
            }
        },
        'MMCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/mmc_btc',
                'fetch_freq': 36000
            }
        },
        'NECBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/nec_btc',
                'fetch_freq': 36000
            }
        },
        'NMCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/nmc_btc',
                'fetch_freq': 36000
            }
        },
        'NXTBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/nxt_btc',
                'fetch_freq': 7200
            }
        },
        'PPCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/ppc_btc',
                'fetch_freq': 36000
            }
        },
        'PRTBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/prt_btc',
                'fetch_freq': 36000
            }
        },
        'PTSBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/pts_btc',
                'fetch_freq': 36000
            }
        },
        'SRCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/src_btc',
                'fetch_freq': 36000
            }
        },
        'TAGBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/tag_btc',
                'fetch_freq': 36000
            }
        },
        'YACBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/yac_btc',
                'fetch_freq': 36000
            }
        },
        'WDCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/wdc_btc',
                'fetch_freq': 36000
            }
        },
        'XCPBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/xcp_btc',
                'fetch_freq': 36000
            }
        },
        'XPMBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/xpm_btc',
                'fetch_freq': 36000
            }
        },
        'ZCCBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/zcc_btc',
                'fetch_freq': 36000
            }
        },
        'ZETBTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/zet_btc',
                'fetch_freq': 36000
            }
        },
        'CENTLTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/cent_ltc',
                'fetch_freq': 36000
            }
        },
        'DVCLTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/dvc_ltc',
                'fetch_freq': 36000
            }
        },
        'IFCLTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/ifc_ltc',
                'fetch_freq': 36000
            }
        },
        'NETLTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/net_ltc',
                'fetch_freq': 36000
            }
        },
        'REDLTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/red_ltc',
                'fetch_freq': 36000
            }
        },
        'TIPSLTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/tips_ltc',
                'fetch_freq': 36000
            }
        },
        'TIXLTC': {
            'tradelist': {
                'url': 'https://data.bter.com/api/1/trade/tix_ltc',
                'fetch_freq': 36000
            }
        },
    },
    'cryptsy': {
        'LTCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 600
            }
        },
        'FTCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 600
            }
        },
        'MNCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'CNCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'RYCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'BQCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'YACBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ELCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'NVCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'WDCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 3600
            }
        },
        'CNCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'WDCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'YACLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'BTBBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'JKCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'DGCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'TRCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'PPCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'NMCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'GLDBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'PXCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'NBLBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'FRKBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'LKYBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'JKCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 26000
            }
        },
        'GLDLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'RYCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'IXCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'FRCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'DVCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'AMCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'FSTBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'MECBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'DBLLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'EZCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ARGBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'BTEBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'BTGBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'SBCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'DVCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'CAPBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'NRBBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'EZCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'MEMLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ALFBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'CRCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'IFCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'IFCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'FLOLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'MSTLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'XPMBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'NANBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'KGCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ANCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'XNCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'CSCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'EMDBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'CGBBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'QRKBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'DMDBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'YBCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'CMCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ORBBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'GLCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'GLXBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'HBNBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'SPTBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'GDCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'STRBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'GMELTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ZETBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'PHSBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'REDLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'SRCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'NECBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'CPRLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'PYCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ELPLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ADTLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'CLRBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'DGCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'SXCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'MECLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'PXCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'BUKBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'XPMLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'TIXLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'NETLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'COLLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ASCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'TEKBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'XJOBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'LK7BTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'TAGBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'PTSBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'PointsBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'ANCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'CGBLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'FSTLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'PPCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'QRKLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ZETLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'SBCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'BETBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'TGCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'DEMBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'DOGEBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 600
            }
        },
        'UNOBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'NETBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'DOGELTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'CATBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'LOTBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'FFCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'EACBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'ZCCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'BCXBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'RPCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'OSCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'MOONLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'TIPSLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'LEAFBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'MEOWBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'CASHBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'VTCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 600
            }
        },
        'MAXBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'SXCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'CACHBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'DRKBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 600
            }
        },
        'MINTBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 3600
            }
        },
        'BENBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'SMCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'NXTBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'AURBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'AURLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'NXTLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'UTCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'MZCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'FLAPBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'TAKBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'DGBBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'SATBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'RDDBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ZEDBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'FRKLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'POTBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'CTMLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ZEITLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'LYCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'KDCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'BCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 3600
            }
        },
        'SPABTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 3600
            }
        },
        'EXEBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'NYANBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'HVCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'BATLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'MN1BTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'EMC2BTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'MRYBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'RBBTLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'BCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'FLTBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'KARMLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'DMCLTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'WCBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 7200
            }
        },
        'MN2BTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 3600
            }
        },
        'CINNIBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'COMMBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
        'ACBTC': {
            'tradelist': {
                'url': 'https://api.cryptsy.com/api',
                'fetch_freq': 36000
            }
        },
    },
    'okcoin': {
        'BTCCNY': {
            'tradelist': {
                'url': 'https://www.okcoin.cn/api/trades.do?symbol=btc_cny',
                'fetch_freq': 60
            }
        },
        'LTCCNY': {
            'tradelist': {
                'url': 'https://www.okcoin.cn/api/trades.do?symbol=ltc_cny',
                'fetch_freq': 60
            }
        }
    },
    'mintpal': {
        'ACBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/AC/BTC',
                'fetch_freq': 600
            }
        },
        'AURBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/AUR/BTC',
                'fetch_freq': 600
            }
        },
        'BCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/BC/BTC',
                'fetch_freq': 600
            }
        },
        'BCLTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/BC/LTC',
                'fetch_freq': 600
            }
        },
        'BTCSBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/BTCS/BTC',
                'fetch_freq': 600
            }
        },
        'C2BTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/C2/BTC',
                'fetch_freq': 600
            }
        },
        'CAIxBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/CAIx/BTC',
                'fetch_freq': 600
            }
        },
        'CINNIBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/CINNI/BTC',
                'fetch_freq': 600
            }
        },
        'CLOAKBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/CLOAK/BTC',
                'fetch_freq': 600
            }
        },
        'COMMBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/COMM/BTC',
                'fetch_freq': 600
            }
        },
        'CRYPTBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/CRYPT/BTC',
                'fetch_freq': 600
            }
        },
        'DGBBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/DGB/BTC',
                'fetch_freq': 600
            }
        },
        'DOGEBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/DOGE/BTC',
                'fetch_freq': 60
            }
        },
        'DOPEBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/DOPE/BTC',
                'fetch_freq': 600
            }
        },
        'DRKBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/DRK/BTC',
                'fetch_freq': 60
            }
        },
        'DRKLTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/DRK/LTC',
                'fetch_freq': 600
            }
        },
        'ECCLTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/ECC/LTC',
                'fetch_freq': 600
            }
        },
        'EMC2BTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/EMC2/BTC',
                'fetch_freq': 600
            }
        },
        'EMOLTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/EMO/LTC',
                'fetch_freq': 600
            }
        },
        'ENCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/ENC/BTC',
                'fetch_freq': 600
            }
        },
        'FACBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/FAC/BTC',
                'fetch_freq': 600
            }
        },
        'FLTBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/FLT/BTC',
                'fetch_freq': 600
            }
        },
        'GRSBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/GRS/BTC',
                'fetch_freq': 600
            }
        },
        'GRUMPLTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/GRUMP/LTC',
                'fetch_freq': 600
            }
        },
        'HIROBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/HIRO/BTC',
                'fetch_freq': 600
            }
        },
        'HVCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/HVC/BTC',
                'fetch_freq': 600
            }
        },
        'ITCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/ITC/BTC',
                'fetch_freq': 600
            }
        },
        'IVCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/IVC/BTC',
                'fetch_freq': 600
            }
        },
        'KARMLTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/KARM/LTC',
                'fetch_freq': 600
            }
        },
        'KDCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/KDC/BTC',
                'fetch_freq': 600
            }
        },
        'LTCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/LTC/BTC',
                'fetch_freq': 60
            }
        },
        'METHBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/METH/BTC',
                'fetch_freq': 600
            }
        },
        'MINTBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/MINT/BTC',
                'fetch_freq': 60
            }
        },
        'MRCLTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/MRC/LTC',
                'fetch_freq': 600
            }
        },
        'MRSBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/MRS/BTC',
                'fetch_freq': 600
            }
        },
        'MYCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/MYC/BTC',
                'fetch_freq': 600
            }
        },
        'MYRBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/MYR/BTC',
                'fetch_freq': 600
            }
        },
        'MZCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/MZC/BTC',
                'fetch_freq': 600
            }
        },
        'NAUTBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/NAUT/BTC',
                'fetch_freq': 600
            }
        },
        'NOBLBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/NOBL/BTC',
                'fetch_freq': 600
            }
        },
        'OLYBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/OLY/BTC',
                'fetch_freq': 600
            }
        },
        'PANDALTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/PANDA/LTC',
                'fetch_freq': 600
            }
        },
        'PENGLTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/PENG/LTC',
                'fetch_freq': 600
            }
        },
        'PNDBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/PND/BTC',
                'fetch_freq': 600
            }
        },
        'PNDLTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/PND/LTC',
                'fetch_freq': 600
            }
        },
        'POTBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/POT/BTC',
                'fetch_freq': 600
            }
        },
        'RICBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/RIC/BTC',
                'fetch_freq': 600
            }
        },
        'RZRBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/RZR/BTC',
                'fetch_freq': 600
            }
        },
        'SATBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/SAT/BTC',
                'fetch_freq': 600
            }
        },
        'SCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/SC/BTC',
                'fetch_freq': 600
            }
        },
        'SPABTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/SPA/BTC',
                'fetch_freq': 600
            }
        },
        'SUPERBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/SUPER/BTC',
                'fetch_freq': 600
            }
        },
        'SYNCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/SYNC/BTC',
                'fetch_freq': 600
            }
        },
        'TAKBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/TAK/BTC',
                'fetch_freq': 600
            }
        },
        'TESBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/TES/BTC',
                'fetch_freq': 600
            }
        },
        'TOPLTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/TOP/LTC',
                'fetch_freq': 600
            }
        },
        'UNOBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/UNO/BTC',
                'fetch_freq': 600
            }
        },
        'USDeBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/USDe/BTC',
                'fetch_freq': 600
            }
        },
        'UTCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/UTC/BTC',
                'fetch_freq': 600
            }
        },
        'VRCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/VRC/BTC',
                'fetch_freq': 60
            }
        },
        'VTCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/VTC/BTC',
                'fetch_freq': 60
            }
        },
        'WCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/WC/BTC',
                'fetch_freq': 600
            }
        },
        'XCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/XC/BTC',
                'fetch_freq': 600
            }
        },
        'XLBBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/XLB/BTC',
                'fetch_freq': 600
            }
        },
        'XMRBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/XMR/BTC',
                'fetch_freq': 600
            }
        },
        'YCBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/YC/BTC',
                'fetch_freq': 600
            }
        },
        'ZEDBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/ZED/BTC',
                'fetch_freq': 600
            }
        },
        'ZEITLTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/ZEIT/LTC',
                'fetch_freq': 600
            }
        },
        'ZETBTC': {
            'tradelist': {
                'url': 'https://api.mintpal.com/v1/market/trades/ZET/BTC',
                'fetch_freq': 600
            }
        }
    },
    'poloniex': {
        'ABYBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_ABY',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_ABY',
                'fetch_freq': 600
            }
        },
        'ACBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_AC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_AC',
                'fetch_freq': 600
            }
        },
        'ADNBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_ADN',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_ADN',
                'fetch_freq': 600
            }
        },
        'AIRBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_AIR',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_AIR',
                'fetch_freq': 600
            }
        },
        'AURBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_AUR',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_AUR',
                'fetch_freq': 600
            }
        },
        'BANKBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_BANK',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_BANK',
                'fetch_freq': 600
            }
        },
        'BCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_BC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_BC',
                'fetch_freq': 600
            }
        },
        'BCCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_BCC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_BCC',
                'fetch_freq': 600
            }
        },
        'BCNBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_BCN',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_BCN',
                'fetch_freq': 600
            }
        },
        'BDCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_BDC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_BDC',
                'fetch_freq': 600
            }
        },
        'BDGBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_BDG',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_BDG',
                'fetch_freq': 600
            }
        },
        'BELABTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_BELA',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_BELA',
                'fetch_freq': 600
            }
        },
        'BITSBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_BITS',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_BITS',
                'fetch_freq': 600
            }
        },
        'BLUBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_BLU',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_BLU',
                'fetch_freq': 600
            }
        },
        'BNSBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_BNS',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_BNS',
                'fetch_freq': 600
            }
        },
        'BONESBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_BONES',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_BONES',
                'fetch_freq': 600
            }
        },
        'CACHBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_CACH',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_CACH',
                'fetch_freq': 600
            }
        },
        'CCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_CC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_CC',
                'fetch_freq': 600
            }
        },
        'CGABTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_CGA',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_CGA',
                'fetch_freq': 600
            }
        },
        'CHABTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_CHA',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_CHA',
                'fetch_freq': 600
            }
        },
        'CINNIBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_CINNI',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_CINNI',
                'fetch_freq': 600
            }
        },
        'CNOTEBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_CNOTE',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_CNOTE',
                'fetch_freq': 600
            }
        },
        'COMMBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_COMM',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_COMM',
                'fetch_freq': 600
            }
        },
        'CUREBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_CURE',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_CURE',
                'fetch_freq': 600
            }
        },
        'DIEMBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_DIEM',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_DIEM',
                'fetch_freq': 600
            }
        },
        'DOGEBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_DOGE',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_DOGE',
                'fetch_freq': 600
            }
        },
        'DRKBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_DRK',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_DRK',
                'fetch_freq': 60
            }
        },
        'DRMBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_DRM',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_DRM',
                'fetch_freq': 600
            }
        },
        'EBTBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_EBT',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_EBT',
                'fetch_freq': 600
            }
        },
        'EFLBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_EFL',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_EFL',
                'fetch_freq': 600
            }
        },
        'EMC2BTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_EMC2',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_EMC2',
                'fetch_freq': 600
            }
        },
        'EXEBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_EXE',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_EXE',
                'fetch_freq': 600
            }
        },
        'FLTBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_FLT',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_FLT',
                'fetch_freq': 600
            }
        },
        'GDNBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_GDN',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_GDN',
                'fetch_freq': 600
            }
        },
        'GIARBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_GIAR',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_GIAR',
                'fetch_freq': 600
            }
        },
        'GLBBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_GLB',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_GLB',
                'fetch_freq': 600
            }
        },
        'GOLDBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_GOLD',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_GOLD',
                'fetch_freq': 600
            }
        },
        'GRCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_GRC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_GRC',
                'fetch_freq': 600
            }
        },
        'GRSBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_GRS',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_GRS',
                'fetch_freq': 600
            }
        },
        'HIROBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_HIRO',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_HIRO',
                'fetch_freq': 600
            }
        },
        'HOTBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_HOT',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_HOT',
                'fetch_freq': 600
            }
        },
        'HUCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_HUC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_HUC',
                'fetch_freq': 600
            }
        },
        'ITCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_ITC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_ITC',
                'fetch_freq': 600
            }
        },
        'IXCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_IXC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_IXC',
                'fetch_freq': 600
            }
        },
        'JUGBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_JUG',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_JUG',
                'fetch_freq': 600
            }
        },
        'LCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_LC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_LC',
                'fetch_freq': 600
            }
        },
        'LCLBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_LCL',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_LCL',
                'fetch_freq': 600
            }
        },
        'LGCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_LGC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_LGC',
                'fetch_freq': 600
            }
        },
        'LTCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_LTC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_LTC',
                'fetch_freq': 60
            }
        },
        'METHBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_METH',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_METH',
                'fetch_freq': 600
            }
        },
        'MINTBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_MINT',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_MINT',
                'fetch_freq': 600
            }
        },
        'MMCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_MMC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_MMC',
                'fetch_freq': 600
            }
        },
        'MNS1BTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_MNS1',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_MNS1',
                'fetch_freq': 600
            }
        },
        'MONBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_MON',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_MON',
                'fetch_freq': 600
            }
        },
        'MRCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_MRC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_MRC',
                'fetch_freq': 600
            }
        },
        'MROBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_MRO',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_MRO',
                'fetch_freq': 600
            }
        },
        'MUNBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_MUN',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_MUN',
                'fetch_freq': 600
            }
        },
        'MYRBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_MYR',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_MYR',
                'fetch_freq': 600
            }
        },
        'NASBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_NAS',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_NAS',
                'fetch_freq': 600
            }
        },
        'NAUTBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_NAUT',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_NAUT',
                'fetch_freq': 600
            }
        },
        'NC2BTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_NC2',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_NC2',
                'fetch_freq': 600
            }
        },
        'NMCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_NMC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_NMC',
                'fetch_freq': 600
            }
        },
        'NOBLBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_NOBL',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_NOBL',
                'fetch_freq': 600
            }
        },
        'NOTEBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_NOTE',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_NOTE',
                'fetch_freq': 600
            }
        },
        'NRSBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_NRS',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_NRS',
                'fetch_freq': 600
            }
        },
        'NXTBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_NXT',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_NXT',
                'fetch_freq': 600
            }
        },
        'PAWNBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_PAWN',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_PAWN',
                'fetch_freq': 600
            }
        },
        'PIGBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_PIG',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_PIG',
                'fetch_freq': 600
            }
        },
        'PLXBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_PLX',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_PLX',
                'fetch_freq': 600
            }
        },
        'PMCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_PMC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_PMC',
                'fetch_freq': 600
            }
        },
        'PPCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_PPC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_PPC',
                'fetch_freq': 600
            }
        },
        'PRCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_PRC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_PRC',
                'fetch_freq': 600
            }
        },
        'PRTBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_PRT',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_PRT',
                'fetch_freq': 600
            }
        },
        'PTSBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_PTS',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_PTS',
                'fetch_freq': 600
            }
        },
        'Q2CBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_Q2C',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_Q2C',
                'fetch_freq': 600
            }
        },
        'QCNBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_QCN',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_QCN',
                'fetch_freq': 600
            }
        },
        'REDDBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_REDD',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_REDD',
                'fetch_freq': 600
            }
        },
        'RICBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_RIC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_RIC',
                'fetch_freq': 600
            }
        },
        'SCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_SC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_SC',
                'fetch_freq': 600
            }
        },
        'SHIBEBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_SHIBE',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_SHIBE',
                'fetch_freq': 600
            }
        },
        'SOCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_SOC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_SOC',
                'fetch_freq': 600
            }
        },
        'SRCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_SRC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_SRC',
                'fetch_freq': 600
            }
        },
        'SYNCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_SYNC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_SYNC',
                'fetch_freq': 600
            }
        },
        'USDEBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_USDE',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_USDE',
                'fetch_freq': 600
            }
        },
        'UVCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_UVC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_UVC',
                'fetch_freq': 600
            }
        },
        'VRCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_VRC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_VRC',
                'fetch_freq': 1800
            }
        },
        'VTCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_VTC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_VTC',
                'fetch_freq': 600
            }
        },
        'WCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_WC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_WC',
                'fetch_freq': 600
            }
        },
        'WDCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_WDC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_WDC',
                'fetch_freq': 600
            }
        },
        'WOLFBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_WOLF',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_WOLF',
                'fetch_freq': 600
            }
        },
        'XBCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_XBC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_XBC',
                'fetch_freq': 600
            }
        },
        'XCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_XC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_XC',
                'fetch_freq': 600
            }
        },
        'XCPBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_XCP',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_XCP',
                'fetch_freq': 600
            }
        },
        'XLBBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_XLB',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_XLB',
                'fetch_freq': 600
            }
        },
        'XPMBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_XPM',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_XPM',
                'fetch_freq': 600
            }
        },
        'XSIBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_XSI',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_XSI',
                'fetch_freq': 600
            }
        },
        'XSVBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_XSV',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_XSV',
                'fetch_freq': 600
            }
        },
        'YACCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_YACC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_YACC',
                'fetch_freq': 600
            }
        },
        'YANGBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_YANG',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_YANG',
                'fetch_freq': 600
            }
        },
        'YCBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_YC',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_YC',
                'fetch_freq': 600
            }
        },
        'YINBTC': {
            'orderbook': 'https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_YIN',
            'tradelist': {
                'url': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_YIN',
                'fetch_freq': 600
            }
        }
    }
}
#        },
#    'allcoin': {
#        'SKCBTC': {
#            'tradelist': {
#                'url': 'https://www.allcoin.com/api1/trade/skc_btc',
#                'trade_freq':
#                }


CRYPTSY_MARKETID = {
    'LTCUSD': 1,
    'BTCUSD': 2,
    'LTCBTC': 3,
    'FTCBTC': 5,
    'FTCUSD': 6,
    'MNCBTC': 7,
    'CNCBTC': 8,
    'RYCBTC': 9,
    'BQCBTC': 10,
    'YACBTC': 11,
    'ELCBTC': 12,
    'NVCBTC': 13,
    'WDCBTC': 14,
    'CNCLTC': 17,
    'WDCLTC': 21,
    'YACLTC': 22,
    'BTBBTC': 23,
    'JKCBTC': 25,
    'DGCBTC': 26,
    'TRCBTC': 27,
    'PPCBTC': 28,
    'NMCBTC': 29,
    'GLDBTC': 30,
    'PXCBTC': 31,
    'NBLBTC': 32,
    'FRKBTC': 33,
    'LKYBTC': 34,
    'JKCLTC': 35,
    'GLDLTC': 36,
    'RYCLTC': 37,
    'IXCBTC': 38,
    'FRCBTC': 39,
    'DVCBTC': 40,
    'AMCBTC': 43,
    'FSTBTC': 44,
    'MECBTC': 45,
    'DBLLTC': 46,
    'EZCBTC': 47,
    'ARGBTC': 48,
    'BTEBTC': 49,
    'BTGBTC': 50,
    'SBCBTC': 51,
    'DVCLTC': 52,
    'CAPBTC': 53,
    'NRBBTC': 54,
    'EZCLTC': 55,
    'MEMLTC': 56,
    'ALFBTC': 57,
    'CRCBTC': 58,
    'IFCBTC': 59,
    'IFCLTC': 60,
    'FLOLTC': 61,
    'MSTLTC': 62,
    'XPMBTC': 63,
    'NANBTC': 64,
    'KGCBTC': 65,
    'ANCBTC': 66,
    'XNCLTC': 67,
    'CSCBTC': 68,
    'EMDBTC': 69,
    'CGBBTC': 70,
    'QRKBTC': 71,
    'DMDBTC': 72,
    'YBCBTC': 73,
    'CMCBTC': 74,
    'ORBBTC': 75,
    'GLCBTC': 76,
    'GLXBTC': 78,
    'HBNBTC': 80,
    'SPTBTC': 81,
    'GDCBTC': 82,
    'STRBTC': 83,
    'GMELTC': 84,
    'ZETBTC': 85,
    'PHSBTC': 86,
    'REDLTC': 87,
    'SRCBTC': 88,
    'NECBTC': 90,
    'CPRLTC': 91,
    'PYCBTC': 92,
    'ELPLTC': 93,
    'ADTLTC': 94,
    'CLRBTC': 95,
    'DGCLTC': 96,
    'SXCLTC': 98,
    'MECLTC': 100,
    'PXCLTC': 101,
    'BUKBTC': 102,
    'XPMLTC': 106,
    'TIXLTC': 107,
    'NETLTC': 108,
    'COLLTC': 109,
    'ASCLTC': 111,
    'TEKBTC': 114,
    'XJOBTC': 115,
    'LK7BTC': 116,
    'TAGBTC': 117,
    'PTSBTC': 119,
    'PointsBTC': 120,
    'ANCLTC': 121,
    'CGBLTC': 123,
    'FSTLTC': 124,
    'PPCLTC': 125,
    'QRKLTC': 126,
    'ZETLTC': 127,
    'SBCLTC': 128,
    'BETBTC': 129,
    'TGCBTC': 130,
    'DEMBTC': 131,
    'DOGEBTC': 132,
    'UNOBTC': 133,
    'NETBTC': 134,
    'DOGELTC': 135,
    'CATBTC': 136,
    'LOTBTC': 137,
    'FFCBTC': 138,
    'EACBTC': 139,
    'ZCCBTC': 140,
    '42BTC': 141,
    'BCXBTC': 142,
    'RPCBTC': 143,
    'OSCBTC': 144,
    'MOONLTC': 145,
    'TIPSLTC': 147,
    'LEAFBTC': 148,
    'MEOWBTC': 149,
    'CASHBTC': 150,
    'VTCBTC': 151,
    'MAXBTC': 152,
    'SXCBTC': 153,
    'CACHBTC': 154,
    'DRKBTC': 155,
    'MINTBTC': 156,
    'BENBTC': 157,
    'SMCBTC': 158,
    'NXTBTC': 159,
    'AURBTC': 160,
    'AURLTC': 161,
    'NXTLTC': 162,
    'UTCBTC': 163,
    'MZCBTC': 164,
    'FLAPBTC': 165,
    'TAKBTC': 166,
    'DGBBTC': 167,
    'SATBTC': 168,
    'RDDBTC': 169,
    'ZEDBTC': 170,
    'FRKLTC': 171,
    'POTBTC': 173,
    'CTMLTC': 175,
    'ZEITLTC': 176,
    'LYCBTC': 177,
    'KDCBTC': 178,
    'BCBTC': 179,
    'SPABTC': 180,
    'DOGEUSD': 182,
    'EXEBTC': 183,
    'NYANBTC': 184,
    'HVCBTC': 185,
    'BATLTC': 186,
    'MN1BTC': 187,
    'EMC2BTC': 188,
    'MRYBTC': 189,
    'RBBTLTC': 190,
    'BCLTC': 191,
    'FLTBTC': 192,
    'KARMLTC': 193,
    'DMCLTC': 194,
    'WCBTC': 195,
    'MN2BTC': 196,
    'CINNIBTC': 197,
    'COMMBTC': 198,
    'ACBTC': 199
}


# number of satoshis in BTC
SATOSHI = 100000000

# number of cents in USD
CENT = 100
