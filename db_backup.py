from pricelogger.database import *
from pricelogger.pricelogger import Session
from config import urls as url

from sqlalchemy.ext.serializer import loads, dumps
import argparse


def backup(session, filename):
    print 'Performing a database backup.'

    for kEx, vEx in zip(url.keys(), url.values()):
        for kPair, vPair in zip(vEx.keys(), vEx.values()):
            for kApi, vApi in zip(vPair.keys(), vPair.values()):

                tradelistClass = eval('%s.%s.%s' % (kEx, kPair, kApi))
                q = session.query(tradelistClass).all()

                data_serialized = dumps(q)
                print data_serialized


def restore(session, filename):
    pass


# TODO: finish this


if __name__ == "__main__":
    #main()
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-b', '--backup', help='backup the database', action='store_true')
    group.add_argument('-r', '--restore', help='restore the database', action='store_true')
    parser.add_argument('-f', '--file', help='Input/Output file')
    args = parser.parse_args()

    session = Session()
    filename = None
    if args.file:
        filename = args.file

    if args.backup:
        backup(session, filename)

    if args.restore:
        restore(session, filename)


