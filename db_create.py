#!/usr/bin/env python

from migrate.versioning import api
import os.path

from config import SQLALCHEMY_MIGRATE_REPO, SQLALCHEMY_DATABASE_URI
from pricelogger.database.base import Base
from pricelogger import engine


print "Creating new database."
Base.metadata.create_all(engine, checkfirst=True)

if not os.path.exists(SQLALCHEMY_MIGRATE_REPO):
    api.create(SQLALCHEMY_MIGRATE_REPO, 'pricelogger database repository')
    api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
else:
    api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, api.version(SQLALCHEMY_MIGRATE_REPO))
