from pricelogger import pricelogger
from config import urls
import sys


try:
    # print "urls['bitstamp']['BTCUSD']['ticker']: ", urls['bitstamp']['BTCUSD']['ticker']
    # pricelogger.run(urls['bitstamp']['BTCUSD']['ticker'])

    pricelogger.run(urls)

except KeyboardInterrupt:

    print "\nExitting."
    sys.exit()
